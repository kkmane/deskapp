package gnukhata;


import gnukhata.views.startupForm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;











//import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

/**
 * @author 
 * Girish Joshi <girish946@gmail.com>, 
 * Krishnakant Mane <kk@dff.org.in>, 
 * Ujwala Pawade <ujwalahpawade@gmail.com>
 * 
 */
/*
 * this class contains all the global data objects required by the gnukhata's
 * frontend.
 */
public class globals
{
	/*
	 * static array contains data objects 0.client_id 1.Organisation name
	 * 2.starting date of the financial year. 3.end date of the financial year.
	 * 4.organisation type. 5.user role.
	 */

	// TODO make appropriate assignment to the elements
//the global array for storing details including clientid for all transactions.
	//this also contains the orgname,financialstart financialend and orgtype.
	public static Object[] session = new Object[9];
	public static Display display;
	public static Image logo;
	public static Image icon;
	public static Image img;
	public static Image img1;
	public static Image backImg;
	public static Image vouchertabImg;
	public static Image voucherbackImg;

	static
	{
		try
		{
			logo = new Image(display, startupForm.class.getResourceAsStream("/images/finallogo1.png"));
		} catch (Exception e)
		{
			System.out.println("Logo not found");
			e.printStackTrace();
		}
		
		try
		{
			backImg = new Image(display, startupForm.class.getResourceAsStream("/images/background.jpg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
		
		try
		{
			vouchertabImg = new Image(display, startupForm.class.getResourceAsStream("/images/background_voucher.jpg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
		
		
		try
		{
			icon = new Image(display, startupForm.class.getResourceAsStream("/images/icon.png"));
		} catch (Exception e)
		{
			System.out.println("Icon not found");
			e.printStackTrace();
		}
		
		try
		{
			img = new Image(display, startupForm.class.getResourceAsStream("/images/kelkar_crop.jpeg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
		try
		{
			img1 = new Image(display, startupForm.class.getResourceAsStream("/images/kk_crop.jpg"));
		} catch (Exception e)
		{
			System.out.println("Image not found");
			e.printStackTrace();
		}
	
	//the xmlrpcclient is named client and used to execute the functions from the rpc server.
	//we will use the execute method of this client to execute any function from core_engine.
	try {
		Runtime r = Runtime.getRuntime();
		String[] command = {"sh","-c","echo $DESKTOP_SESSION"};
		Process proc = r.exec(command);

		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader bf = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String s = null;
		while ((s = bf.readLine()) != null) {
			System.out.println(s);
			session[8] = s;
		}
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

	public static XmlRpcClient client;
	
	static
	{
		try
		{
			client = new XmlRpcClient();
			XmlRpcClientConfigImpl conf = new XmlRpcClientConfigImpl();
			conf.setServerURL(new URL("http://localhost:7081"));
			conf.setEnabledForExtensions(true);
			client.setConfig(conf);


		} catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
	}
	/*public static void main(String[] arts) {
	

		// Vector<Object> params = new Vector<Object>();
		 Object[] params = new Object[] {};

		try
		{
			Object[] result = (Object[]) globals.client.execute("getOrganisationNames", params);
			//client.call("calculate",5,5,5 );
			System.out.println("success");
			for (int i = 0; i < result.length; i++)
			{
				System.out.println(result[i].toString());

			}
			System.out.println(result.length);
		} 
		catch (Exception e)
		{
			e.printStackTrace();

		}
	}*/
	//function for coloring the group on focus events.
	public static void SetTextColoredFocusEvents(Composite form,final Color FocusBackground, final Color FocusForeground, final Color Background, final Color Foreground)
	{
		Control[]widgets =   form.getChildren();
		//form.setBackground(new Color(this.getDisplay() ,0,0,255));
		//form.setForeground(new Color(this.getDisplay() ,0, 0,0 ));
		for(int colorcounter = 0; colorcounter < widgets.length; colorcounter ++)
		{
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
			{

				continue;
				
			}
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Text"))
			{
				final Text CurrentText = (Text) widgets[colorcounter];
				
				CurrentText.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						CurrentText.setBackground(FocusBackground);
						CurrentText.setForeground(FocusForeground);
						CurrentText.selectAll();

					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						CurrentText.setBackground(Background);
						CurrentText.setForeground(Foreground);
					}
				});
			}
		}

	}
	public static void SetButtonColoredFocusEvents(Composite form,final Color FocusBackground, final Color BtnFocusForeground, final Color Background, final Color Foreground)
	{
		Control[]widgets =   form.getChildren();
		//form.setBackground(new Color(this.getDisplay() ,0,0,255));
		//form.setForeground(new Color(this.getDisplay() ,0, 0,0 ));
		for(int colorcounter = 0; colorcounter < widgets.length; colorcounter ++)
		{
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
			{

				continue;
				
			}
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Button"))
			{
				final Button CurrentButton = (Button) widgets[colorcounter];
				
				CurrentButton.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						CurrentButton.setBackground(FocusBackground);
						CurrentButton.setForeground(BtnFocusForeground);

					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						CurrentButton.setBackground(Background);
						CurrentButton.setForeground(Foreground);
					}
				});
			}
		}

	}
	public static void SetComboColoredFocusEvents(Composite form,final Color FocusBackground, final Color FocusForeground, final Color Background, final Color Foreground)
	{
		Control[]widgets =   form.getChildren();
		//form.setBackground(new Color(this.getDisplay() ,0,0,255));
		//form.setForeground(new Color(this.getDisplay() ,0, 0,0 ));
		for(int colorcounter = 0; colorcounter < widgets.length; colorcounter ++)
		{
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
			{

				continue;
				
			}
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Combo"))
			{
				final Combo CurrentCombo = (Combo) widgets[colorcounter];
				
				CurrentCombo.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						CurrentCombo.setBackground(FocusBackground);
						CurrentCombo.setForeground(FocusForeground);

					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						CurrentCombo.setBackground(Background);
						CurrentCombo.setForeground(Foreground);
					}
				});
			}
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.custom.CCombo"))
			{
				final CCombo CurrentCombo = (CCombo) widgets[colorcounter];
				
				CurrentCombo.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						CurrentCombo.setBackground(FocusBackground);
						CurrentCombo.setForeground(FocusForeground);

					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						CurrentCombo.setBackground(Background);
						CurrentCombo.setForeground(Foreground);
					}
				});
			}
		}

	}
	public static void SetTableColoredFocusEvents(Composite form,final Color FocusBackground, final Color FocusForeground, final Color Background, final Color Foreground)
	{
		Control[]widgets =   form.getChildren();
		//form.setBackground(new Color(this.getDisplay() ,0,0,255));
		//form.setForeground(new Color(this.getDisplay() ,0, 0,0 ));
		for(int colorcounter = 0; colorcounter < widgets.length; colorcounter ++)
		{
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
			{

				continue;
				
			}
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Table"))
			{
				final Table CurrentTable = (Table) widgets[colorcounter];
				
				CurrentTable.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						CurrentTable.setBackground(FocusBackground);
						CurrentTable.setForeground(FocusForeground);

					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						CurrentTable.setBackground(Background);
						CurrentTable.setForeground(Foreground);
					}
				});
			}
		}

	}
	
	public static void setThemeColor(Composite form, Color Background, Color Foreground)
	{
		Control[]widgets =   form.getChildren();
		//form.setBackground(new Color(this.getDisplay() ,0,0,255));
		//form.setForeground(new Color(this.getDisplay() ,0, 0,0 ));
		for(int colorcounter = 0; colorcounter < widgets.length; colorcounter ++)
		{
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
			{

				continue;
				
			}
			
			if(widgets[colorcounter].getClass().getName().equals("org.eclipse.swt.widgets.Group"))
					{
				Group  CurrentGroup = (Group) widgets[colorcounter];
				Control[] GroupChildren = CurrentGroup.getChildren();
				for(int grpcounter = 0; grpcounter < GroupChildren.length; grpcounter++)
				{
					if(GroupChildren[grpcounter].getClass().getName().equals("org.eclipse.swt.widgets.Label"))
					{

						MessageBox msgtest = new MessageBox(new Shell(),SWT.OK);
						msgtest.setMessage(widgets[colorcounter].getClass().getName());
						//msgtest.open();
						continue;
						
					}
					
					GroupChildren[grpcounter].setBackground(Background); 
GroupChildren[grpcounter].setForeground(Foreground );
				}
					}
				
			widgets[colorcounter].setBackground(Background);
			widgets[colorcounter].setForeground(Foreground) ;
			
		}

		

		}

	
}
