package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.accountController;

import java.text.NumberFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

//import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

public class FindAndEditAccountComposite extends Composite 
{
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;

	double newvalue;
	double oldvalue;
	//boolean editamt1=false;
	//boolean focusflag=false;
	//boolean editamt=false;
//	boolean onceEdited = false;
	boolean VerifyFlag = false;
	//boolean enterflag=false;
	
	String accountCode = "";
	String searchText = "";
	private long searchtextTimeout = 0;
	
	NumberFormat nf = NumberFormat.getInstance();
	public static Label lblsavemsg;
	static Display display;
	TabItem tiAddNewAccount;
	Composite cmpAddNewAccount;
	Composite newAComposite;
	Label lblcrdrblnc;
	Label lblGroupName;
	Label txtGroupName;
	Label lblSubGroupName;
	Label txtSubGroupName;
	Label lblAccountName;
	Text txtAccountName;
	Label lblOpeningBalance; 
	Text txtOpeningBalance;
	Label lblTotalDrOpeningBalance;
	Label txtTotalDrOpeningBalance ;
	Label lblTotalCrOpeningBalance;
	Label txtTotalCrOpeningBalance;
	Label lblDiffInOpeningBalance;
	Label txtDiffInOpeningBalance;
	CCombo dropdownAllAccounts;
	Button btnSearch;
	Button btnEdit;
	Button btnConfirm;
	Button btnDelete;
	TabItem tiEditAccount;
	Label lblrupeesymbol;
	String oldvalue1;
	String newvalue1;
	//Label lblrupeesymbol1;
	//Label lblrupeesymbo2;

	public FindAndEditAccountComposite(Composite parent, int style) {
		super(parent,style);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(false);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		// TODO Auto-generated constructor stub
		
		lblsavemsg = new Label(this, SWT.NONE);
		lblsavemsg.setFont(new Font(display, "Time New Roman", 14, SWT.BOLD|SWT.COLOR_RED));
		FormData layout = new FormData();
		layout.top = new FormAttachment(12);
		layout.left = new FormAttachment(27);
		layout.right = new FormAttachment(93);
		layout.bottom = new FormAttachment(18);
		lblsavemsg.setLayoutData(layout);
	
	   
		 lblAccountName = new Label(this, SWT.NONE);
		 lblAccountName.setText("A&ccount Name :");
		 lblAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		 layout = new FormData();
		 layout.top = new FormAttachment(20);
		 layout.left = new FormAttachment(2);
		 layout.right = new FormAttachment(18);
		 layout.bottom = new FormAttachment(25);
		 lblAccountName.setLayoutData(layout);
			 
			dropdownAllAccounts = new CCombo(this, SWT.BORDER| SWT.READ_ONLY);
			dropdownAllAccounts.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(19);
			layout.left = new FormAttachment(18);
			layout.right = new FormAttachment(40);
			layout.bottom = new FormAttachment(25);
			dropdownAllAccounts.setLayoutData(layout);
			//this.dropdownAllAccounts.setFocus();
			
			btnSearch = new Button(this, SWT.NONE);
			btnSearch.setText("&Search");
			btnSearch.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(19);
			layout.left = new FormAttachment(41);
			layout.right = new FormAttachment(48);
			layout.bottom = new FormAttachment(25);
			btnSearch.setLayoutData(layout);
			btnSearch.setVisible(false);
			
			lblGroupName = new Label(this, SWT.NONE);
			lblGroupName.setText("Group Name :");
			lblGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(29);
			layout.left = new FormAttachment(2);
		/*	layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(34);
		*/	lblGroupName.setLayoutData(layout);
			
			txtGroupName = new Label(this, SWT.BORDER | SWT.READ_ONLY);
			txtGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(28);
			layout.left = new FormAttachment(18);
			layout.right = new FormAttachment(40);
			layout.bottom = new FormAttachment(34);
			txtGroupName.setLayoutData(layout);
			
			lblSubGroupName = new Label(this, SWT.NONE);
			lblSubGroupName.setText("Sub-Group Name :");
			lblSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(38);
			layout.left = new FormAttachment(2);
			/*layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(43);
			*/lblSubGroupName.setLayoutData(layout);
			//txtSubGroupName.setEditable(false); 
			
			txtSubGroupName = new Label(this, SWT.BORDER |SWT.READ_ONLY);
			txtSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(37);
			layout.left = new FormAttachment(18);
			layout.right = new FormAttachment(40);
			layout.bottom = new FormAttachment(43);
			txtSubGroupName.setLayoutData(layout);
			
			lblAccountName = new Label(this, SWT.NONE);
			lblAccountName.setText("A&ccount Name :");
			lblAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(47);
			layout.left = new FormAttachment(2);
			/*layout.right = new FormAttachment(24);
			layout.bottom = new FormAttachment(52);
			*/lblAccountName.setLayoutData(layout);
			
			txtAccountName = new Text(this, SWT.BORDER|SWT.READ_ONLY);
			txtAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(46);
			layout.left = new FormAttachment(18);
			layout.right = new FormAttachment(40);
			layout.bottom = new FormAttachment(52);
			txtAccountName.setLayoutData(layout);
			txtAccountName.setEnabled(false);
					
			lblOpeningBalance = new Label(this,SWT.NONE );
			lblOpeningBalance.setText("&Opening Balance (₹) :");
			lblOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(2);
			/*layout.right = new FormAttachment(26);
			layout.bottom = new FormAttachment(61);
			*/lblOpeningBalance.setLayoutData(layout);
			
			txtOpeningBalance = new Text(this, SWT.RIGHT| SWT.READ_ONLY | SWT.BORDER);
			//txtOpeningBalance.setMessage("0.00");
			txtOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(55);
			layout.left = new FormAttachment(18);
			layout.right = new FormAttachment(40);
			layout.bottom = new FormAttachment(61);
			txtOpeningBalance.setLayoutData(layout);
			txtOpeningBalance.setText("0.00");
			txtOpeningBalance.setEnabled(false);
			
			
			lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
			lblTotalDrOpeningBalance.setText("Total Debit Opening Balance:");
			lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(20);
			layout.left = new FormAttachment(58);
			/*layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(25);
			*/lblTotalDrOpeningBalance.setLayoutData(layout);
			lblTotalDrOpeningBalance.setVisible(false);
			
			txtTotalDrOpeningBalance = new Label(this, SWT.RIGHT|SWT.BORDER|SWT.READ_ONLY);
			nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			nf.setGroupingUsed(false);
			txtTotalDrOpeningBalance.setText(nf.format( accountController.getTotalDr()));
			//txtTotalDrOpeningBalance.setMessage("0.00");
			txtTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(20);
			layout.left = new FormAttachment(78);
			layout.right = new FormAttachment(90);
			//layout.bottom = new FormAttachment(34);
			txtTotalDrOpeningBalance.setLayoutData(layout);
			txtTotalDrOpeningBalance.setVisible(false);
			
			/*lblrupeesymbol = new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(28);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(34);
			lblrupeesymbol.setLayoutData(layout);
			*/
			lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
			lblTotalCrOpeningBalance.setText("Total Credit Opening Balance:");
			lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(38);
			layout.left = new FormAttachment(58);
			/*layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(43);
			*/lblTotalCrOpeningBalance.setLayoutData(layout);
			lblTotalCrOpeningBalance.setVisible(false);
			
			txtTotalCrOpeningBalance = new Label(this, SWT.RIGHT|SWT.BORDER|SWT.READ_ONLY);
			txtTotalCrOpeningBalance.setText(nf.format(accountController.getTotalCr() ) );
			//txtTotalCrOpeningBalance.setMessage("0.00");
			txtTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(38);
			layout.left = new FormAttachment(78);
			layout.right = new FormAttachment(90);
			//layout.bottom = new FormAttachment(52);
			txtTotalCrOpeningBalance.setLayoutData(layout);
			txtTotalCrOpeningBalance.setVisible(false);
			
			/*lblrupeesymbol= new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(46);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(52);
			lblrupeesymbol.setLayoutData(layout);
		*/
			lblDiffInOpeningBalance = new Label(this, SWT.NONE);
			lblDiffInOpeningBalance.setText("Difference in Opening Balance:");
			lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(58);
			/*layout.right = new FormAttachment(98);
			layout.bottom = new FormAttachment(61);
			*/lblDiffInOpeningBalance.setLayoutData(layout);
			lblDiffInOpeningBalance.setVisible(false);
			
			lblcrdrblnc = new Label(this, SWT.NONE);
			lblcrdrblnc.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
			double balDiff = accountController.getTotalDr() - accountController.getTotalCr();
			Double val=balDiff;
			if(val>0)
			{
				lblcrdrblnc.setText("Dr");
			}
			else if (val<0) 
			{
				lblcrdrblnc.setText("Cr");
			}
			else
			{
				lblcrdrblnc.setText("");
			}
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(92);
			/*layout.right = new FormAttachment(74);
			layout.bottom = new FormAttachment(70);
			*/lblcrdrblnc.setLayoutData(layout);
			lblcrdrblnc.setVisible(false);
			
			txtDiffInOpeningBalance = new Label(this, SWT.RIGHT|SWT.BORDER|SWT.READ_ONLY);
			if(val < 0 )
			{
				val = Math.abs(val);
			}
			txtDiffInOpeningBalance.setText(nf.format(Double.parseDouble(val.toString())));
			txtDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(56);
			layout.left = new FormAttachment(78);
			layout.right = new FormAttachment(90);
			//layout.bottom = new FormAttachment(70);
			txtDiffInOpeningBalance.setLayoutData(layout);
			txtDiffInOpeningBalance.setVisible(false);
			
			/*lblrupeesymbol = new Label(this, SWT.NONE);
			lblrupeesymbol.setText("(₹):");
			lblrupeesymbol.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
			layout = new FormData();
			layout.top = new FormAttachment(64);
			layout.left = new FormAttachment(91);
			layout.right = new FormAttachment(95);
			layout.bottom = new FormAttachment(70);
			lblrupeesymbol.setLayoutData(layout);
		*/
		
		btnEdit = new Button(this, SWT.NONE);
		btnEdit.setText("&Edit");
		btnEdit.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(28);
		layout.bottom = new FormAttachment(94);
		btnEdit.setEnabled(false);
		btnEdit.setLayoutData(layout);
		
		btnConfirm = new Button(this, SWT.NONE);
		btnConfirm.setText("Conf&irm");
		btnConfirm.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(32);
		layout.right = new FormAttachment(42);
		layout.bottom = new FormAttachment(94);
		btnConfirm.setEnabled(false);
		btnConfirm.setLayoutData(layout) ;
		
		btnDelete = new Button(this, SWT.NONE);
		btnDelete.setText("&Delete");
		btnDelete.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(94);
		btnDelete.setEnabled(false);
		btnDelete.setLayoutData(layout);
		
		layout=new FormData();
		layout.top = new FormAttachment(87);
		layout.left = new FormAttachment(69);
		layout.right = new FormAttachment(77);
		layout.bottom=new FormAttachment(96);
				
		String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
		dropdownAllAccounts.add("----------Please select----------");
		dropdownAllAccounts.select(0);
		
		for (int i = 0; i < allAccounts.length; i++ )
		{
			dropdownAllAccounts.add(allAccounts[i]);
			
		}
		//ListViewer lv = new ListViewer(dropdownAllAccounts);
		
		this.setEvents();
		this.pack();
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);

		globals.setThemeColor(this, Background, Foreground);
	    globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
        globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

	this.makeaccssible(this);
	
			
		}
	private void setEvents()
	{
		this.txtAccountName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
		//		focusflag=true;
			}
			public void focusLost(FocusEvent arg0) {
				
			
				try {
					newvalue1=(txtAccountName.getText().trim());
				} catch (NumberFormatException e1) {
					
				}
				
			}
		});
		dropdownAllAccounts.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if (((e.stateMask & SWT.ALT)== SWT.ALT) && (e.keyCode == SWT.ARROW_DOWN)) 
				{
				dropdownAllAccounts.setListVisible(true);	
				}
				if(e.keyCode == SWT.CR|| e.keyCode == SWT.KEYPAD_CR)
				{
					if(dropdownAllAccounts.getSelectionIndex()== 0 )
					{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR | SWT.ICON_ERROR);
					errMessage.setText("Error!");
					errMessage.setMessage("Please select an account");
					errMessage.open();
					dropdownAllAccounts.setFocus();
					//txtAccountName.setText("");
					return;
					
			}
					
					
					dropdownAllAccounts.setListVisible(false);
					
				}
				
			}
		});
		btnEdit.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
				
				if(btnDelete.isEnabled())
				{
					if(e.keyCode==SWT.ARROW_RIGHT)
					{
						btnDelete.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					btnEdit.setEnabled(false);
					
					/*txtGroupName.setText("");
					txtSubGroupName.setText("");
					txtAccountName.setText("");
					txtOpeningBalance.setText("");*/
					
				if(!dropdownAllAccounts.isEnabled() && e.keyCode==SWT.ARROW_UP)
				{
					//e.doit = false;
					//return;
					btnEdit.setEnabled(true);
					btnEdit.setFocus();	
				}
				/*else
				{
					dropdownAllAccounts.setFocus();
				}*/
			}}
		});
		btnDelete.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e)
			{
					if(e.keyCode==SWT.ARROW_LEFT)
					{
						btnEdit.setFocus();
					}
				
			}
		});
		dropdownAllAccounts.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg0){
				
				
				dropdownAllAccounts.setListVisible(true);
				
				
				if(! lblsavemsg.getText().trim().equals(""))
				{	
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 1000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
					
							lblsavemsg.setText("");

						}
				});

					
				}

			//dropdownFinancialYear.setFocus();
			return;
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
			//	super.focusLost(arg0);
				dropdownAllAccounts.setListVisible(false);
			}
		});
		
		/*dropdownAllAccounts.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0){
				
				dropdownAllAccounts.setListVisible(false);
		}
		});
*/
		dropdownAllAccounts.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				//code here
				if(arg0.keyCode== SWT.CR | arg0.keyCode== SWT.KEYPAD_CR)
				{
						
						//MessageBox msg = new MessageBox(new Shell(),SWT.OK);
						//msg.setMessage("Enter clicked");
						//msg.open();
						if(dropdownAllAccounts.getSelectionIndex() > 0)
						{
							
							btnSearch.notifyListeners(SWT.Selection, new Event());
							btnEdit.setFocus();
						}
						//dropdownAllAccounts.setListVisible(false);
						
					
					dropdownAllAccounts.notifyListeners(SWT.Selection ,new Event()  );
					//dropdownFinancialYear.setFocus();
					return;
				}
				/*if(!Character.isLetterOrDigit(arg0.character) )
				{
					return;
				}
				
				*/
				
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				long now = System.currentTimeMillis();
				if (now > searchtextTimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(arg0.character);
				searchtextTimeout = now + 500;
				
				for(int i = 0; i < dropdownAllAccounts.getItemCount(); i++ )
				{
					if(dropdownAllAccounts.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownAllAccounts.select(i);
						dropdownAllAccounts.notifyListeners(SWT.Selection, new Event());
						break;
					}
					
				}
			}
		});
		
		
		
		dropdownAllAccounts.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//super.widgetSelected(e);
				//make a call to the controler for getting account details.
				
				
				
				if(dropdownAllAccounts.getSelectionIndex()== 0 )
				{
					txtAccountName.setText("");
					txtGroupName.setText("");
					txtOpeningBalance.setText("");
					txtSubGroupName.setText("");
					btnEdit.setEnabled(false);
					return;
				}
				
				
				String[] details = accountController.getAccountDetails(2, dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()));
				txtGroupName.setText(details[1]);
				if(details[2].equals("0.0"))
				{
					txtSubGroupName.setText("");
				}
				else
				{
					txtSubGroupName.setText(details[2]);
				}
					txtAccountName.setEnabled(false);
				txtAccountName.setText(details[3]);
				if(txtGroupName.getText().trim().equals("Direct Income")|| txtGroupName.getText().trim().equals("Direct Expense") || txtGroupName.getText().trim().equals("Indirect Income") || txtGroupName.getText().trim().equals("Indirect Expense") )
				{
					txtOpeningBalance.setEnabled(false);
					txtOpeningBalance.setText("0.00");
				}
				else
				{
					txtOpeningBalance.setEnabled(false);
					try {
						
						txtOpeningBalance.setText(nf.format(Double.parseDouble(details[4].toString())));
						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.getMessage();
					}

				}
				accountCode = details[0];
				if(details[5].equals("false") &&details[6].equals("false"))
				{
					btnDelete.setEnabled(true);
				}
				else
				{
					btnDelete.setEnabled(false);
				}
				//btnEdit.setEnabled(true);
				//btnEdit.setFocus();
				
				btnEdit.setEnabled(true);
				
				if (dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit & Loss") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Income & Expenditure")) {
					btnEdit.setEnabled(false);
					btnDelete.setEnabled(false);
				}
				
				if (dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Opening Stock") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Closing Stock") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Loss c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Loss b/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit b/f") ||  dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Deficit c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Deficit b/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Surplus c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Surplus b/f")|| dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Stock at the Beginning"))
				{
					btnDelete.setEnabled(false);
				}
				
			}
		});
		btnSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			//	super.keyPressed(arg0);
				if(arg0.keyCode== SWT.ARROW_LEFT)
				{
					dropdownAllAccounts.setFocus();
				}
				if(arg0.keyCode== SWT.ARROW_DOWN)
				{
					btnEdit.setFocus();
				}
			}
		});
		txtGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				dropdownAllAccounts.setFocus();
			}
		});
		txtSubGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				dropdownAllAccounts.setFocus();
			}
		});
		btnDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//super.widgetSelected(e);
				MessageBox delConfirm = new MessageBox(new Shell(),SWT.YES | SWT.NO | SWT.ICON_QUESTION);
				//CustomDialog delConfirm= new CustomDialog(new Shell());
				delConfirm.setMessage("You are about to delete "+ txtAccountName.getText().trim()+" account");
				int answer = delConfirm.open();
				if( answer == SWT.YES)
				{
					accountController.deleteAccount(txtAccountName.getText().trim());
					btnDelete.setEnabled(false);
					btnEdit.setEnabled(false);
					btnConfirm.setEnabled(false);
					dropdownAllAccounts.setEnabled(true);
					//btnSearch.setVisible(true);
					txtGroupName.setText("");
					txtSubGroupName.setText("");
					txtAccountName.setText("");
					//txtOpeningBalance.setText("");
					dropdownAllAccounts.removeAll();
					String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
					dropdownAllAccounts.add("----------Please Select----------");
					dropdownAllAccounts.select(0);
					for (int i = 0; i < allAccounts.length; i++ )
					{
						dropdownAllAccounts.add(allAccounts[i]);
						
					}	
					dropdownAllAccounts.setFocus();

					
				}
			}
		});
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit & Loss") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Income & Expenditure")) {
					MessageBox msg = new MessageBox(new Shell(),SWT.OK|SWT.ICON_INFORMATION);
					msg.setMessage("This Account cannot be edited.");
					dropdownAllAccounts.setFocus();
					msg.open();
					return;
				}
				
				btnConfirm.setEnabled(true);
				if (dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Opening Stock") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Closing Stock") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Loss c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Loss b/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Profit b/f") ||  dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Deficit c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Deficit b/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Surplus c/f") || dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Surplus b/f")|| dropdownAllAccounts.getItem(dropdownAllAccounts.getSelectionIndex()).trim().equals("Stock at the Beginning")) 
				{
				txtAccountName.setEnabled(false);
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//txtAccountName.setFocus();
						txtOpeningBalance.setEnabled(true);
						txtOpeningBalance.setFocus();
						txtOpeningBalance.selectAll();
						btnEdit.setEnabled(false);
						
						}
				});
								
				}
				else
				{
					
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							//txtAccountName.setFocus();
							txtAccountName.setEnabled(true);
							txtAccountName.setEditable(true);
							txtAccountName.setFocus();
							txtAccountName.selectAll();
							btnEdit.setEnabled(false);
						}
					});
					
				}
				
				txtOpeningBalance.setEditable(true);
				oldvalue1 = txtAccountName.getText().trim();
				oldvalue = Double.parseDouble(txtOpeningBalance.getText().trim());
				btnEdit.setEnabled(false);
				dropdownAllAccounts.setEnabled(false);
				btnSearch.setVisible(false);
				if(txtGroupName.getText().trim().equals("Direct Income")|| txtGroupName.getText().trim().equals("Direct Expense") || txtGroupName.getText().trim().equals("Indirect Income") || txtGroupName.getText().trim().equals("Indirect Expense") )
				{
					txtOpeningBalance.setEnabled(false);
					txtTotalCrOpeningBalance.setVisible(false);
					txtTotalDrOpeningBalance.setVisible(false);
					lblTotalCrOpeningBalance.setVisible(false);
					lblTotalDrOpeningBalance.setVisible(false);
					lblDiffInOpeningBalance.setVisible(false);
					txtDiffInOpeningBalance.setVisible(false);
					lblcrdrblnc.setVisible(false);
				}
				else
				{
					txtOpeningBalance.setEnabled(true);
					txtTotalCrOpeningBalance.setVisible(true);
					txtTotalDrOpeningBalance.setVisible(true);
					lblTotalCrOpeningBalance.setVisible(true);
					lblTotalDrOpeningBalance.setVisible(true);
					lblDiffInOpeningBalance.setVisible(true);
					txtDiffInOpeningBalance.setVisible(true);
					lblcrdrblnc.setVisible(true);
				}
				
				
				
				
				/*Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//txtAccountName.setFocus();
						txtAccountName.selectAll();
						txtAccountName.setFocus();
						btnEdit.setEnabled(false);
					}
				});*/
			}
		});
		txtAccountName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				
				
				
					
				if(arg0.keyCode== SWT.CR | arg0.keyCode== SWT.KEYPAD_CR)
				{
					/*if(enterflag==true)
					{
						editamt=true;
					}
					else if(enterflag==false)
					{
						editamt=false;
					}*/
					
					if(txtOpeningBalance.isEnabled())
					{
						txtOpeningBalance.setFocus();
					}
					else
					{	
						btnConfirm.setFocus();
						btnConfirm.notifyListeners(SWT.Selection, new Event()  );
					}
					
					/*if((arg0.keyCode ==SWT.ARROW_RIGHT) && (oldvalue1==newvalue1))
					{
					enterflag=false;	
					}*/
				}
				else
				{
					//enterflag=true;
					//editamt=true;
				}				
			}
		});
		
		
		txtOpeningBalance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				
				
				
										
				if(arg0.keyCode== SWT.CR | arg0.keyCode==SWT.KEYPAD_CR)
				{
					btnConfirm.setFocus();
					btnConfirm.notifyListeners(SWT.Selection,new Event() );
				}
				else if(arg0.keyCode==SWT.ARROW_UP)
				{
						
					txtAccountName.setFocus();
				
				}
				
			}
		});
		txtOpeningBalance.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(VerifyFlag == false)
				{
					arg0.doit= true;
					return;
				}
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode==45)
				{
					return;
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.KEYPAD_DECIMAL:
	                return;
	        }

	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }

				

				
			}
		});

		
		this.txtOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				
				//VerifyFlag = true;
			}
			public void focusLost(FocusEvent arg0){
				//VerifyFlag=false;
				
				
				/*if(oldvalue==newvalue)
				{
					editamt1=false;
					
				}
				else if(oldvalue!=newvalue)
				{
					
					editamt1=true;
				}*/
				
				try {
					txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText().trim())));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					txtOpeningBalance.setText("0.00");
						
				//	e.printStackTrace();
				}
				try {
					newvalue=Double.parseDouble(txtOpeningBalance.getText().trim());
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					//txtOpeningBalance.setText("0.00");
				}
			}
		});
		
		btnConfirm.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e){
								
				
				if(txtAccountName.getText().trim().equals("") )
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK|SWT.ERROR | SWT.ICON_ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please enter Account Name");
					errMessage.open();
					txtAccountName.setFocus();
					return;
				}
				//if(! txtOpeningBalance.getEnabled()|| txtOpeningBalance.getText().trim().equals(""))
				if(txtOpeningBalance.getText().trim().equals(""))
				{
					txtOpeningBalance.setText("0.00");
				}
				if (!oldvalue1.trim().equalsIgnoreCase(txtAccountName.getText().trim())) {
					String result = accountController
							.accountExists(txtAccountName.getText().trim());
					if (Integer.valueOf(result) == 1) {
						MessageBox msg = new MessageBox(new Shell(), SWT.OK
								| SWT.ERROR | SWT.ICON_ERROR);
						msg.setText("Warning!");
						msg.setMessage("The account name you entered already exists, please choose another name.");
						msg.open();

						Display.getCurrent().asyncExec(new Runnable() {

							@Override
							public void run() {
					
								// TODO Auto-generated method stub
								txtAccountName.setText("");
								txtAccountName.setFocus();

							}
						});
						return;
					}
				}
				if (txtAccountName.getText().trim().equals("Profit for the Year")||txtAccountName.getText().trim().equals("Loss for the Year")||txtAccountName.getText().trim().equals("Surplus for the Year")||txtAccountName.getText().trim().equals("Deficit for the Year"))
				{
					MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_ERROR);
					msg.setText("Warning!");
					msg.setMessage("This is a reserved account name, please choose another name.");
					txtAccountName.setText("");
					txtAccountName.setFocus();
					msg.open();
					return;
				}
				//onceEdited = true;
				if(txtGroupName.getText().trim().equals("Direct Income")|| txtGroupName.getText().trim().equals("Direct Expense") || txtGroupName.getText().trim().equals("Indirect Income") || txtGroupName.getText().trim().equals("Indirect Expense") )
				{
					if((oldvalue1.equals(newvalue1)))
					{
						
						txtAccountName.setEnabled(false);
						txtOpeningBalance.setEnabled(false);
						
						btnEdit.setEnabled(true);
						dropdownAllAccounts.setEnabled(true);
						btnConfirm.setEnabled(false);
						//btnSearch.setVisible(true);
						dropdownAllAccounts.setFocus();
						
						return;
					}
				}
				else
				{
					if((oldvalue1.equals(newvalue1)) && (oldvalue == newvalue))
					{
						
						txtAccountName.setEnabled(false);
						txtOpeningBalance.setEnabled(false);
						
						btnEdit.setEnabled(true);
						dropdownAllAccounts.setEnabled(true);
						btnConfirm.setEnabled(false);
						//btnSearch.setVisible(true);
						dropdownAllAccounts.setFocus();
						
						return;
					}
			
				}
						
			//if(editamt1==true | editamt==true)
				
					
					
		 //   MessageBox confirm = new MessageBox(new Shell(),SWT.YES| SWT.NO|SWT.ICON_QUESTION);
			CustomDialog confirm= new CustomDialog(new Shell());
			confirm.SetMessage("Do you want to save?");
			int answer=confirm.open();
			if(answer==SWT.YES)
			{
				try {
					double newOpeningBalance = accountController.editAccount(txtAccountName.getText().trim(),accountCode, txtGroupName.getText().trim(),Double.valueOf(nf.format(Double.valueOf(txtOpeningBalance.getText().trim()))));
					//txtOpeningBalance.setText(nf.format(newOpeningBalance));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				txtAccountName.setEditable(true);
				lblsavemsg.setText("Account edited successfully");
				lblsavemsg.setVisible(true);
				txtDiffInOpeningBalance.setVisible(false);
				txtTotalCrOpeningBalance.setVisible(false);
				txtTotalDrOpeningBalance.setVisible(false);
				lblDiffInOpeningBalance.setVisible(false);
				lblTotalCrOpeningBalance.setVisible(false);
				lblTotalDrOpeningBalance.setVisible(false);
				lblcrdrblnc.setVisible(false);
			/*MessageBox confirmMessage = new MessageBox(new Shell(),SWT.OK|SWT.ICON_INFORMATION);
			confirmMessage.setText("Information");
			confirmMessage.setMessage("Account edited successfully");
			confirmMessage.open();
			*/btnEdit.setEnabled(false);
			btnConfirm.setEnabled(false);
			btnDelete.setEnabled(false);
			dropdownAllAccounts.setEnabled(true);
			//btnSearch.setVisible(true);
		//	enterflag= false;
			dropdownAllAccounts.removeAll();
			String[] allAccounts = gnukhata.controllers.accountController.getAllAccounts();
			dropdownAllAccounts.add("----------Please select----------");
			dropdownAllAccounts.select(0);
			for (int i = 0; i < allAccounts.length; i++ )
			{
				dropdownAllAccounts.add(allAccounts[i]);
				
			}	
			dropdownAllAccounts.setFocus();
			//VerifyFlag = false;
			//txtOpeningBalance.setText("0.00");
			txtAccountName.setText("");
			txtGroupName.setText("");
			txtSubGroupName.setText("");
			txtOpeningBalance.setText("0.00");
			txtAccountName.setEnabled(false);
			txtOpeningBalance.setEnabled(false);
			

			Double totalDrOpeningBalance = 0.00;
			Double totalCrOpeningBalance = 0.00;
			Double diffBalance = 0.00;
			totalCrOpeningBalance= accountController.getTotalCr();
			totalDrOpeningBalance = accountController.getTotalDr();
			txtTotalCrOpeningBalance.setText(nf.format(totalCrOpeningBalance));
			txtTotalDrOpeningBalance.setText( nf.format(totalDrOpeningBalance));
			diffBalance = totalDrOpeningBalance - totalCrOpeningBalance;
			if(diffBalance < 0 )
			{
				diffBalance = Math.abs(diffBalance);
			}
			Double val=	diffBalance;
			txtDiffInOpeningBalance.setText(nf.format( Double.parseDouble(val.toString())));
			if(totalDrOpeningBalance>totalCrOpeningBalance)
			{
				lblcrdrblnc.setText("Dr");
			}
			else if (totalCrOpeningBalance>totalDrOpeningBalance) 
			{
				lblcrdrblnc.setText("Cr");
			}
			else
			{
				lblcrdrblnc.setText("");
			}
			}
			else
			{
				dropdownAllAccounts.notifyListeners(SWT.Selection, new Event());
				//btnSearch.setVisible(true);
				btnConfirm.setEnabled(false);
				dropdownAllAccounts.setEnabled(true);
				dropdownAllAccounts.setFocus();
				txtDiffInOpeningBalance.setVisible(false);
				txtTotalCrOpeningBalance.setVisible(false);
				txtTotalDrOpeningBalance.setVisible(false);
				lblDiffInOpeningBalance.setVisible(false);
				lblTotalCrOpeningBalance.setVisible(false);
				lblTotalDrOpeningBalance.setVisible(false);
				lblcrdrblnc.setVisible(false);
				return;
			}
			
			}
			/*else if(editamt==false && editamt1 ==false)
				{
					MessageBox noedit= new MessageBox(new Shell(),SWT.OK | SWT.ICON_INFORMATION);
					noedit.setText("Information");
					noedit.setMessage("Nothing edited");
					txtAccountName.setEditable(false);
					txtOpeningBalance.setEditable(false);
					noedit.open();
					btnEdit.setEnabled(true);
					btnConfirm.setEnabled(false);
					Display.getCurrent().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							btnEdit.setFocus();
														
						}
					});
					
				}
			*/
		});
		
		txtTotalCrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{					
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}
			}
		} );
		txtTotalDrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}

			}
		});
		txtDiffInOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(btnConfirm.getEnabled())
				{
					btnConfirm.setFocus();
				}
				else
				{
					dropdownAllAccounts.setFocus();
				}
			}
		});

	}
	
	
	
		public void makeaccssible(Control c)
			{
				c.getAccessible();
			}
		
	}