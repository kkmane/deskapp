package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportmodels.conventionalbalancesheet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;

public class ViewBalanceSheetReport extends Composite {
	
	TableViewer tbLiaBalanceSheet;
	TableViewer tbAssetBalancesheet;
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color tableBackground;
	Color tableForeground;
	Color lightBlue;
	TableViewerColumn colCapitalLiabilities;
	TableViewerColumn colAmount1;
	TableViewerColumn colAmount2;
	TableViewerColumn colPropertyAssets;
	TableViewerColumn colAmount3;
	TableViewerColumn colAmount4;
	
	ODPackage sheetStream;
		
	int tblia_width;
	int tbas_width;
	int tblIndexLia;
	int tblIndexAss;
	
	Label lbllia_accname;
	Label lblassets_accname;
	Label lblasset_tolamt;
	Label lbllia_tolamt;
	Label lbllia_amt;
	Label lblasset_amt;
	Button btnBacktoBalanceSheet;
	Button btnPrint;
	
	TableEditor lia_accnameEditor;
	TableEditor lia_tolamtEditor;
	TableEditor assets_accnameEditor;
	TableEditor asset_tolamtEditor;
	TableEditor lia_amtEditor;
	TableEditor asset_amtEditor;
	
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = ""; 
	NumberFormat nf;
	static Display display;
	String strOrgType;
	Vector<Object> printconventinal = new Vector<Object>();
	
	public ViewBalanceSheetReport(Composite parent, int style,String endDate,ArrayList<conventionalbalancesheet>convbaldata_asset,ArrayList<conventionalbalancesheet>convbaldata_liabilities) {
		// TODO Auto-generated constructor stub
	
		super(parent,style);
		
		strOrgType = globals.session[4].toString();
		
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText(" Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);


		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);
		

		

		
				
		Label lblheadline=new Label(this, SWT.NONE);
		String strdate="";
		strdate=endDate.substring(8)+" - "+endDate.substring(5,7)+" - "+endDate.substring(0, 4);
		endDateParam = endDate;
		if(globals.session[4].equals("Profit Making"))
		 {
			lblheadline.setText("Balance Sheet as on "+strdate);
		 }
		if(globals.session[4].equals("NGO"))
		 {
			lblheadline.setText("Statement of Affairs as on "+strdate);
		 }
		lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		layout.left = new FormAttachment(40);
		layout.bottom = new FormAttachment(8);
		lblheadline.setLayoutData(layout);
			
		tbLiaBalanceSheet= new TableViewer(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbLiaBalanceSheet.getTable().setLinesVisible (true);
		tbLiaBalanceSheet.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,10);
		layout.left = new FormAttachment(0);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(91);
		tbLiaBalanceSheet.getTable().setLayoutData(layout);
		

		tbAssetBalancesheet= new TableViewer(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbAssetBalancesheet.getTable().setLinesVisible(true);
		tbAssetBalancesheet.getTable().setHeaderVisible(true);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,10);
		layout.left = new FormAttachment(50);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(91);
		tbAssetBalancesheet.getTable().setLayoutData(layout);
		
		btnBacktoBalanceSheet =new Button(this,SWT.PUSH);
		if(globals.session[4].equals("Profit Making"))
		 {
			btnBacktoBalanceSheet.setText("&Back");
		 }
		if(globals.session[4].equals("NGO"))
		 {
			btnBacktoBalanceSheet.setText("&Back");
		 }
		btnBacktoBalanceSheet.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbAssetBalancesheet.getTable(), 20);
		layout.left=new FormAttachment(25);
		btnBacktoBalanceSheet.setLayoutData(layout);
		btnBacktoBalanceSheet.setFocus();
			

		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbAssetBalancesheet.getTable(),20);
		layout.left=new FormAttachment(50);
		btnPrint.setLayoutData(layout);
		
		
		
		this.makeaccessible(tbLiaBalanceSheet.getTable());
		this.getAccessible();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		tblia_width = tbLiaBalanceSheet.getTable().getClientArea().width;
		tbas_width = tbAssetBalancesheet.getTable().getClientArea().width;
		/*MessageBox m = new MessageBox(new Shell(),SWT.OK);
		m.setMessage(Integer.toString(shellwidth));
		m.open();*/
		
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/ConventionalBalSheet.ots"),"ConventionalBalSheet");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
			Background =  new Color(this.getDisplay() ,220 , 224, 227);
			Foreground = new Color(this.getDisplay() ,0, 0,0 );
			FocusBackground  = new Color(this.getDisplay(),78,97,114 );
			FocusForeground = new Color(this.getDisplay(),255,255,255);
			tableBackground = new Color(this.getDisplay(),255, 255, 214);
			tableForeground =  new Color(this.getDisplay(),184, 255, 148);
			lightBlue = new Color(this.getDisplay(),215,242,251);
			globals.setThemeColor(this, Background, Foreground);
			tbAssetBalancesheet.getControl().setBackground(lightBlue);
			tbLiaBalanceSheet.getControl().setBackground(lightBlue);
	        globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
			globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
	       // globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
			globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
			//tbLiaBalanceSheet.getTable().setBackground(FocusBackground);
			//tbLiaBalanceSheet.getTable().setForeground(FocusForeground);
			
		this.setReport(convbaldata_asset,convbaldata_liabilities);
		this.setEvents(convbaldata_asset,convbaldata_liabilities);
		
	}
	private void setReport(ArrayList<conventionalbalancesheet>convbaldata_asset,ArrayList<conventionalbalancesheet>convbaldata_liabilities)
	{
		
		colCapitalLiabilities = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		if(globals.session[4].equals("Profit Making"))
		{ 
		colCapitalLiabilities.getColumn().setText("Capital and Liabilities");
		}
		if(globals.session[4].equals("NGO"))
		 {
			colCapitalLiabilities.getColumn().setText("Corpus and Liabilities");
		 }
		colCapitalLiabilities.getColumn().setAlignment(SWT.LEFT);
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colCapitalLiabilities.getColumn().setWidth(45 * tblia_width /100);
		}
		else
		{
			colCapitalLiabilities.getColumn().setWidth(47 * tblia_width /100);
		}
		
		colCapitalLiabilities.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet caplia = (conventionalbalancesheet) element;
		return caplia.getCapitalLiabilities();
		}	
		});
		
		colAmount1 = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		colAmount1.getColumn().setText("Amount");
		colAmount1.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colAmount1.getColumn().setWidth(25 * tblia_width/100);	
		}
		else
		{
			colAmount1.getColumn().setWidth(25 * tblia_width/100);
		}
		
		colAmount1.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet amt1 = (conventionalbalancesheet) element;
		try {
			if(amt1.getAmount1().equals("")){
				return "";
			}
			Double amount1 = Double.parseDouble(amt1.getAmount1());
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount1);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		
		
		colAmount2 = new TableViewerColumn(tbLiaBalanceSheet,SWT.None);
		colAmount2.getColumn().setText("Amount");
		colAmount2.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colAmount2.getColumn().setWidth(25 * tblia_width/100);	
		}
		else
		{
			colAmount2.getColumn().setWidth(25 * tblia_width/100);
		}
		colAmount2.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet amt2 = (conventionalbalancesheet) element;
		try {
			if(amt2.getAmount2().equals("")){
				return "";
			}
			Double amount2 = Double.parseDouble(amt2.getAmount2());
			if(amount2.equals("") && ! amt2.getCapitalLiabilities().equals(""))
			{
				amount2=0.00;
			}
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount2);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		
		
		colPropertyAssets = new TableViewerColumn(tbAssetBalancesheet,SWT.None);
		colPropertyAssets.getColumn().setText("Property and Assets");
		colPropertyAssets.getColumn().setAlignment(SWT.LEFT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colPropertyAssets.getColumn().setWidth(45 * tbas_width /100);
		}
		else
		{
			colPropertyAssets.getColumn().setWidth(47 * tbas_width /100);
		}
		
		colPropertyAssets.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet propass = (conventionalbalancesheet) element;
		return propass.getPropertyAssets();
		}	
		});
		
		colAmount3 = new TableViewerColumn(tbAssetBalancesheet,SWT.None);
		colAmount3.getColumn().setText("Amount");
		colAmount3.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colAmount3.getColumn().setWidth(25 * tblia_width/100);	
		}
		else
		{
			colAmount3.getColumn().setWidth(25 * tblia_width/100);
		}
		colAmount3.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet amt3 = (conventionalbalancesheet) element;
		try {
			if(amt3.getAmount3().equals("")){
				return "";
			}
			Double amount3 = Double.parseDouble(amt3.getAmount3());
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount3);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		
		
		colAmount4 = new TableViewerColumn(tbAssetBalancesheet,SWT.None);
		colAmount4.getColumn().setText("Amount");
		colAmount4.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			colAmount4.getColumn().setWidth(25 * tblia_width/100);	
		}
		else
		{
			colAmount4.getColumn().setWidth(25 * tblia_width/100);
		}
		
		colAmount4.setLabelProvider(new ColumnLabelProvider()
		{
		@Override
		public String getText(Object element) {
			// TODO Auto-generated method stub
		gnukhata.controllers.reportmodels.conventionalbalancesheet amt4 = (conventionalbalancesheet) element;
		try {
			if(amt4.getAmount4().equals("")){
				return "";
			}
			Double amount4 = Double.parseDouble(amt4.getAmount4());
			if(amount4.equals("") && ! amt4.getPropertyAssets().equals(""))
			{
				amount4=0.00;
			}
			
			nf = NumberFormat.getInstance();
			nf.setGroupingUsed(false);
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			return nf.format(amount4);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
		
		}	
		});
		tbLiaBalanceSheet.setContentProvider(new ArrayContentProvider());
		tbLiaBalanceSheet.setInput(convbaldata_liabilities);
		
		tbAssetBalancesheet.setContentProvider(new ArrayContentProvider());
		tbAssetBalancesheet.setInput(convbaldata_asset);
		TableItem[] items = tbAssetBalancesheet.getTable().getItems();
		for (int rowid=0; rowid<items.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items[rowid].setBackground(tableBackground);
		    }
		    else {
		    	items[rowid].setBackground(tableForeground);
		    }
		}
		TableItem[] items1 = tbLiaBalanceSheet.getTable().getItems();
		for (int rowid=0; rowid<items1.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items1[rowid].setBackground(tableBackground);
		    }
		    else {
		    	items1[rowid].setBackground(tableForeground);
		    }
		}
		tbLiaBalanceSheet.getTable().setSelection(0);
		tbLiaBalanceSheet.getTable().setFocus();
	}
	
	private void setEvents(final ArrayList<conventionalbalancesheet>convbaldata_asset,final ArrayList<conventionalbalancesheet>convbaldata_liabilities)
	{
	btnBacktoBalanceSheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnBacktoBalanceSheet.getParent().getParent();
				btnBacktoBalanceSheet.getParent().dispose();
					
				viewBalanceSheet bs = new viewBalanceSheet(grandParent,SWT.NONE);
				bs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
	
		tbLiaBalanceSheet.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndexLia=tbLiaBalanceSheet.getTable().getSelectionIndex();
				tbLiaBalanceSheet.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tbLiaBalanceSheet.getTable().setSelection(tblIndexLia);
			}
		});
		
		tbAssetBalancesheet.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndexAss=tbAssetBalancesheet.getTable().getSelectionIndex();
				tbAssetBalancesheet.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tbAssetBalancesheet.getTable().setSelection(tblIndexAss);
			}
		});
		
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				//String[] orgname=(String[])btnPrint.getData("printorgname");
				//String[] columns = (String[]) btnPrint.getData("printcolumns");
				//Object[][] orgdata =new Object[printconventinal.size()][orgname.length ];
				//Object[][] finalData =new Object[printconventinal.size()][columns.length];
				btnBacktoBalanceSheet.setFocus();
				try {
						final File conventionalbs = new File("/tmp/gnukhata/Report_Output/ConventionalBalanceSheet" );
						final Sheet conventionalSheet = sheetStream.getSpreadSheet().getFirstSheet();
						conventionalSheet.ensureRowCount(100000);
						conventionalSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						conventionalSheet.getCellAt(0, 1).setValue("Balance Sheet As At "+ globals.session[3].toString());
						for (int rowcounter=0; rowcounter<convbaldata_asset.size(); rowcounter++)
						{
							
							conventionalSheet.getCellAt(0, rowcounter+3).setValue(convbaldata_liabilities.get(rowcounter).getCapitalLiabilities());
							conventionalSheet.getCellAt(1, rowcounter+3).setValue(convbaldata_liabilities.get(rowcounter).getAmount1());
							conventionalSheet.getCellAt(2, rowcounter+3).setValue(convbaldata_liabilities.get(rowcounter).getAmount2());
							conventionalSheet.getCellAt(3, rowcounter+3).setValue(convbaldata_asset.get(rowcounter).getPropertyAssets());
							conventionalSheet.getCellAt(4, rowcounter+3).setValue(convbaldata_asset.get(rowcounter).getAmount3());
							conventionalSheet.getCellAt(5, rowcounter+3).setValue(convbaldata_asset.get(rowcounter).getAmount4());
						}
						OOUtils.open(conventionalSheet.getSpreadSheet().saveAs(conventionalbs));

						//OOUtils.open(conventionalbs);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
	}

		
		public void makeaccessible(Control c) {
		/*
		 * getAccessible() method is the method of class Control which is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}



}