package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportmodels.cashflowReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;

public class ViewCashFlowReport extends Composite {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color lightBlue;
	Color tabalternate; 
    Color tabalternate1;

    int tblfocusindex;
    
	String strOrgName;
	String strToYear;
	ODPackage sheetStream;
	TableViewer CashFlowTable;
	TableViewerColumn AccName;
	TableViewerColumn Amount;
	TableViewerColumn AccName1;
	TableViewerColumn Amount1;
	
	
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblaccname;
	Label lblaccname1;
	Label lblamount;
	Label lblamount1;
	Label lblPeriod;
	Label lblstmt;
	Label lblReceipt;
	Label lblPayment;
	Label lblorgname;
	Button btnViewcashflowForAccount;
	//Button btnPrint;

	
	TableEditor AccnameEditor;
	TableEditor AmountEditor;
	TableEditor AccnameEditor1;
	TableEditor AmountEditor1;
	
	//TableEditor VoucherNumberEditor;
	//TableEditor DrEditotr;
	
static Display display;
	
	Button btnViewLedgerForAccount;
	Button btnPrint;
	Color bgbtnColor;
	Color fgbtnColor;
	
	
	
	static String frmDate;
	String strFromDate;
	String strToDate;
	String toDate;
	String endDateParam = "";
	Vector<Object> printcashflow = new Vector<Object>();
	int shellwidth = 0;
	int finshellwidth;
	public ViewCashFlowReport(Composite parent, int style,ArrayList<cashflowReport> cashreport, String fromDate, String toDate, String financialFrom) {
		// TODO Auto-generated constructor stub
		
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		String strdate;
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		endDateParam = toDate;

		
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText(" Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);


		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);
		

		

		//strToDate=fromDate.substring(8)+"-"+fromDate.substring(5,7)+"-"+fromDate.substring(0,4);
		
		Label lblstmt = new Label(this, SWT.NONE);
		lblstmt.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblLine,1);
		
		//layout.right = new FormAttachment(85);
		layout.bottom = new FormAttachment(8);
		
		if(globals.session[4].equals("Profit Making"))
		 {
			lblstmt.setText("CASH FLOW FOR THE PERIOD ENDED  "+ strdate);
			layout.left = new FormAttachment(35);
		 }
		 if(globals.session[4].equals("NGO"))
			{
			 lblstmt.setText("RECEIPT AND PAYMENT ACCOUNT FOR THE PERIOD ENDED  "+ strdate);
			 layout.left = new FormAttachment(25);
			}
		
		
		lblstmt.setLayoutData(layout);
		
		Label lblReceipt = new Label(this, SWT.NONE);
		lblReceipt.setText(" RECEIPTS ");
		lblReceipt.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC|SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblstmt,8);
		layout.left = new FormAttachment(25);
		//layout.right = new FormAttachment(19);
		layout.bottom = new FormAttachment(11);
		lblReceipt.setLayoutData(layout);
		
		Label lblPayment = new Label(this, SWT.NONE);
		lblPayment.setText(" PAYMENTS ");
		lblPayment.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC |SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblstmt,8);
		layout.left = new FormAttachment(64);
//		layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(11);
		lblPayment.setLayoutData(layout);
		
		
		if (globals.session[8].toString().equals("ubuntu")) {
			CashFlowTable = new TableViewer(this, SWT.MULTI | SWT.BORDER
					| SWT.LINE_SOLID | SWT.FULL_SELECTION);
			CashFlowTable.getTable().setLinesVisible(true);
			CashFlowTable.getTable().setHeaderVisible(true);
			layout = new FormData();
			layout.top = new FormAttachment(lblReceipt, 5);
			layout.left = new FormAttachment(8);
			layout.right = new FormAttachment(88);
			layout.bottom = new FormAttachment(85);
			CashFlowTable.getTable().setLayoutData(layout);
			CashFlowTable.getTable().forceFocus();
		}
		else
		{
			CashFlowTable = new TableViewer(this, SWT.MULTI | SWT.BORDER
					| SWT.LINE_SOLID | SWT.FULL_SELECTION);
			CashFlowTable.getTable().setLinesVisible(true);
			CashFlowTable.getTable().setHeaderVisible(true);
			layout = new FormData();
			layout.top = new FormAttachment(lblReceipt, 5);
			layout.left = new FormAttachment(8);
			layout.right = new FormAttachment(88);
			layout.bottom = new FormAttachment(78);
			CashFlowTable.getTable().setLayoutData(layout);
			CashFlowTable.getTable().forceFocus();
		}
		
		btnViewcashflowForAccount =new Button(this,SWT.PUSH);
		btnViewcashflowForAccount.setText("&Back");
		btnViewcashflowForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(CashFlowTable.getTable(),10);
		layout.left=new FormAttachment(25);
		btnViewcashflowForAccount.setLayoutData(layout);
		btnViewcashflowForAccount.setFocus();

		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(CashFlowTable.getTable(),10);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		this.makeaccessible(CashFlowTable.getTable());
		this.getAccessible();
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		shellwidth = this.getClientArea().width;
		finshellwidth = shellwidth-(shellwidth*20/100);
		try {
			sheetStream =  ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/CashFlow.ots"),"CashFlow");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tabalternate =  new Color(this.getDisplay(),255, 255, 214);
		tabalternate1 =  new Color(this.getDisplay(),184, 255, 148);
		
		
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		lightBlue = new Color(this.getDisplay(),215,242,251);
		globals.setThemeColor(this, Background, Foreground);
		CashFlowTable.getControl().setBackground(lightBlue);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		//globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

		this.setReport(cashreport);
		
		this.setEvents(cashreport);
	
	
		
	//this.pack();
	}
	
	private void setReport(ArrayList<cashflowReport> cashreport)
	{
		AccName = new TableViewerColumn(CashFlowTable, SWT.None);
		AccName.getColumn().setText("                                    Account Name");
		//AccName.getColumn().setAlignment(SWT.CENTER);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			AccName.getColumn().setWidth(34 * finshellwidth /100);
		}
		else
		{
			AccName.getColumn().setWidth(32* finshellwidth /100);
		}
		AccName.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.cashflowReport cashacc = (gnukhata.controllers.reportmodels.cashflowReport) element;
			return cashacc.getAccName();
			}
		});

		Amount = new TableViewerColumn(CashFlowTable, SWT.None);
		Amount.getColumn().setText("Amounts                         ");
		Amount.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			Amount.getColumn().setWidth(16 * finshellwidth /100);
		}
		else
		{
			Amount.getColumn().setWidth(18 * finshellwidth /100);
		}
		Amount.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.cashflowReport cashamt = (gnukhata.controllers.reportmodels.cashflowReport) element;
			return cashamt.getAmounts();
			}
		});
		
		AccName1 = new TableViewerColumn(CashFlowTable, SWT.None);
		AccName1.getColumn().setText("                                    Account Name");
		//AccName1.getColumn().setAlignment(SWT.CENTER);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			AccName1.getColumn().setWidth(34 * finshellwidth /100);
		}
		else
		{
			AccName1.getColumn().setWidth(32 * finshellwidth /100);
		}
		AccName1.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.cashflowReport cashacc = (gnukhata.controllers.reportmodels.cashflowReport) element;
			return cashacc.getAccName1();
			}
		});
		
		Amount1 = new TableViewerColumn(CashFlowTable, SWT.None);
		Amount1.getColumn().setText("Amounts                         ");
		Amount1.getColumn().setAlignment(SWT.RIGHT);
		
		if(globals.session[8].toString().equals("ubuntu"))
		{
			Amount1.getColumn().setWidth(16 * finshellwidth /100);
		}
		else
		{
			Amount1.getColumn().setWidth(18 * finshellwidth /100);
		}
		Amount1.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.cashflowReport cashamt = (gnukhata.controllers.reportmodels.cashflowReport) element;
			return cashamt.getAmounts1();
			}
			
		});
		
		CashFlowTable.setContentProvider(new ArrayContentProvider());
		CashFlowTable.setInput(cashreport);
		
		TableItem[] items1 = CashFlowTable.getTable().getItems();
		for (int rowid=0; rowid<items1.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items1[rowid].setBackground(tabalternate);
		    }
		    else {
		    	items1[rowid].setBackground(tabalternate1);
		    }
		}


		
		CashFlowTable.getTable().setSelection(0);
		CashFlowTable.getTable().setFocus();
	}
	
	private void setEvents(final ArrayList<cashflowReport> cashreport)
	{
		
		CashFlowTable.getTable().addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				tblfocusindex = CashFlowTable.getTable().getSelectionIndex();
				CashFlowTable.getTable().setSelection(-1);
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				CashFlowTable.getTable().setSelection(tblfocusindex);
			}
		});
		
		btnViewcashflowForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewcashflowForAccount.getParent().getParent();
				btnViewcashflowForAccount.getParent().dispose();
					
					viewCashflow vl=new viewCashflow(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			
			}
		
		});
		
		//CashFlowTable.setFocus();
		
		btnViewcashflowForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
				    btnPrint.setFocus();
				}
					
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewcashflowForAccount.setFocus();
				}
			}
		});

		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				btnViewcashflowForAccount.setFocus();
					try {
						
						
						final File cashflow = new File("/tmp/gnukhata/Report_Output/cashflow" );
						final Sheet cashSheet = sheetStream.getSpreadSheet().getFirstSheet();;
						cashSheet.ensureRowCount(100000);
						cashSheet.getCellAt(0,0).setValue(globals.session[1].toString());
						cashSheet.getCellAt(0,1).setValue("Receipt And Payment Account For The Period Ended "+globals.session[3].toString());
						for(int rowcounter=0; rowcounter<cashreport.size(); rowcounter++)
						{
							
							cashSheet.getCellAt(0,rowcounter+4).setValue(cashreport.get(rowcounter).getAccName());
							cashSheet.getCellAt(1,rowcounter+4).setValue(cashreport.get(rowcounter).getAmounts());
							cashSheet.getCellAt(2,rowcounter+4).setValue(cashreport.get(rowcounter).getAccName1());
							cashSheet.getCellAt(3,rowcounter+4).setValue(cashreport.get(rowcounter).getAmounts1());
						}
						OOUtils.open(cashSheet.getSpreadSheet().saveAs(cashflow));

						//OOUtils.open(cashflow);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		
	}
		
	public void makeaccessible(Control c) {
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}

	
	
	
		
	
}