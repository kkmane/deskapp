package gnukhata.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

/*
 * @authors 
 * Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>
 */

public class WelcomeComposite extends Composite {

	Label gkidea;
	Label website;
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public WelcomeComposite(Composite parent, int style) {
		super(parent, style);
		 GridLayout gridLayout = new GridLayout();
			gridLayout.numColumns = 1;
			this.setLayout(gridLayout);
			GridData gridData =new GridData(GridData.FILL, GridData.CENTER, true, false);
			gridData.horizontalSpan = 3;
			gkidea=new Label(this,SWT.NONE);
			gkidea.setText("GNUKhata A Free And Open Source Accounting Software");
			gkidea.setForeground(new Color(this.getParent().getDisplay(),255,0,0));
			website=new Label(this,SWT.CENTER);
			website.setText("					http://gnukhata.org				");
			website.setForeground(new Color(this.getParent().getDisplay(), 150,230,40));
			Label l1=new Label(this,SWT.CENTER);
			l1.setText("Shortcuts are now enabled, go to \"Help > Shortcut Keys\" to see the various Shortcuts");
			l1.setForeground(new Color(this.getParent().getDisplay(),0,0,255));
			Label l2 =new Label(this,SWT.CENTER);
			l2.setText("A detailed version of Help is available in the Help section of the menu bar");
			l2.setForeground(new Color(this.getParent().getDisplay(),0,0,255));
			Label l3=new Label(this,SWT.CENTER);
			l3.setText("Currently Funded By National Mission For Education Through ICT(NMEICT)");
			l3.setForeground(new Color(this.getParent().getDisplay(),0,0,255));
			Label l4=new Label(this,SWT.CENTER);
			l4.setText("Contact us for reporting any bugs, queries or complaints regarding the software");
			l4.setForeground(new Color(this.getParent().getDisplay(),0,0,255));
			this.makeaccssible(this);
	}

	 public void makeaccssible(Control c)
		{
			c.getAccessible();
		}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
