package gnukhata.views;




import gnukhata.globals;
import gnukhata.controllers.StartupController;
import gnukhata.controllers.reportController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/*
 * @authors 
 *T Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>
 * * Girish Mane <girishmane8692@gmail.com>
 * * Vinay Khedekar <vinay.itengg@gmail.com>
 * * Sayali Mane <sayali@dff.org.in>
 * * Ujwala Pawade <ujwalahpawade@gmail.com>
 * * Krishnakant Mane <kk@dff.org.in>	
 */

public class MainShell extends Shell
{
	 static Menu menubar;	
	 static Menu master;
	 int counter;

	 static MenuItem mastermenu;
	 
	static Label lblLogo;
	static Label lblLine;
	static Label lblOrgDetails;
	Composite grandParent;

	static MenuItem create_account;
	static MenuItem edit_org;
	static MenuItem add_proj;
	static MenuItem bank_rec_statement;
	 
	static Menu transactions;
	static MenuItem transactionsmenu;
	static MenuItem Contra;                              
	static MenuItem   Journal;                              
	static MenuItem   Payment;                          
	static MenuItem   Receipt;                                  
	static MenuItem   Credit_Note;                                
	static MenuItem   Debit_Note;                                
	static MenuItem   Sales;                                
	static MenuItem   Sales_Return;                                
	static MenuItem   Purchase;                                
	static MenuItem   Purchase_Return;
	static MenuItem   FindVoucher;

	static Menu reports;
	static MenuItem reportsmenu;
	static MenuItem Ledger;
	static MenuItem Trial_Balance;
	static MenuItem Project_Statement;
	static MenuItem Cash_Flow;
	static MenuItem Balance_Sheet;
	static MenuItem Profit_and_Loss_Account;
	static MenuItem Account_List;

	 
	static Menu admin;
	static MenuItem adminmenu;
	static MenuItem New_User;
	static MenuItem Remove_User;
	static MenuItem Change_Password;                                    
	static MenuItem Roll_Over; 
	static MenuItem Delete_Organisation;
	   
	static Menu help;
	static Menu sessionbtn;
	static MenuItem sbtn;
	static MenuItem helpmenu;
	static MenuItem About_GNUKhata;
	static MenuItem ChangeOrg;
	static MenuItem Change_User;
	static MenuItem Quit;
	static MenuItem Authors;                        
	static MenuItem Shortcut_Keys;                            
	static MenuItem GNUKhata_License;                     
	static MenuItem GNUKhata_Manual;
	static ToolItem Purchase_Rerurn;
	static ToolItem tiAccount;
	static ToolItem tiAccountReport;
	static ToolItem tiLedger;
	static ToolItem tiContra;
	static ToolItem tiPayment;
	static ToolItem tiReceipt;
	static ToolItem tiJournal;
	static ToolItem tiSales;
	static ToolItem tiPurchase;
	static ToolItem tiCredit;
	static ToolItem tiDebit;
	static ToolItem tiSalesReturn;
	static ToolItem tiPurchaseReturn;
	static ToolItem tiTrialBalance;
	static ToolItem tiPnl;
	static ToolItem tiBalanceSheet;
	static ToolItem tiCashFlow;
	static ToolItem tiProjectStatement;
	static ToolItem tiBankReco;
	static ToolItem tiEditOrg;
	static ToolItem tiAddProject;
	static ToolItem tiFindVoucher;
	static ToolItem tiLogout;
	static ToolItem tiChange_org;
	static ToolItem tiQuit;
	
	Label lblorgname;
	 //Button logout;
	
	 Composite currentcomposite;
	 Composite basecomposite;
	 
	 static String strOrgName;
	 static String strFromYear;
	 static String strToYear;
	 static String strtype;
	 Label lblRegiNo;
	 Label lblnote;
	 Label lblDelOrgMsg;
	 Color white;
	 int flag=0;
	 			protected Shell shell;
	 Composite formArea;
	 static ToolBar tb;
	 ProgressBar selectbar;
	
	 
	public MainShell(Display display)
	{	
		super(display,SWT.SHELL_TRIM );
		this.setFullScreen(true);
		FormLayout	  fl = new FormLayout();
		Rectangle bounds = this.getDisplay().getPrimaryMonitor().getBounds();
		this.setBounds(bounds);
		
		
		this.setLayout(fl);
		this.setText("GNUKhata");
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		
		FormLayout formLayout= new FormLayout();
		this.setLayout(formLayout);
	    FormData layout=new FormData();
	    
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(strOrgName.replace("&", "&&")+ "\n"+" Financial Year "+"From "+strFromYear+" To "+strToYear);
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(7);
		lblOrgDetails.setLayoutData(layout);
		
		
		lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(56);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(9);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display, "finallogo1.png");
		lblLogo.setImage(globals.logo);
			
		lblLine = new Label(this, SWT.NONE);
		lblLine.setText("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(12);
		lblLine.setLayoutData(layout);
		
		lblDelOrgMsg = new Label(this, SWT.NONE);
		lblDelOrgMsg.setText("Deleting Organisation.\n     "
				+ "Please Wait...");
		lblDelOrgMsg.setFont(new Font(display, "Times New Roman", 20, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(30);
		lblDelOrgMsg.setLayoutData(layout);
		lblDelOrgMsg.setVisible(false);
		
		selectbar=new ProgressBar(this, SWT.SMOOTH);
		selectbar.setVisible(false);
		layout = new FormData();
		layout.top = new FormAttachment(lblDelOrgMsg,10);
		layout.left = new FormAttachment(25);
		layout.right = new FormAttachment(65);
		selectbar.setLayoutData(layout);
		
	//initialise menus.
		menubar = new Menu(this,SWT.BAR);
		sessionbtn=new Menu(menubar); 
		master=new Menu(menubar);

		 transactions=new Menu(menubar);
		 reports=new Menu(menubar);
		 admin=new Menu(menubar);
		 help=new Menu(menubar);
		 
		 sbtn=new MenuItem(menubar, SWT.CASCADE);
		 sbtn.setText("&GNUKhata");
		 sbtn.setMenu(sessionbtn);
		 
		 mastermenu=new MenuItem(menubar,SWT.CASCADE);
		 mastermenu.setText("&Master");
		 mastermenu.setMenu(master);
		 
		 transactionsmenu=new MenuItem(menubar,SWT.CASCADE);
		
				 
		 reportsmenu=new MenuItem(menubar,SWT.CASCADE);
		 reportsmenu.setText("&Reports");
		 reportsmenu.setMenu(reports);
		 
		 adminmenu=new MenuItem(menubar,SWT.CASCADE);
		 adminmenu.setText("&Administration");
		 adminmenu.setMenu(admin);
		 
		 helpmenu=new MenuItem(menubar,SWT.CASCADE);
		 helpmenu.setText("&Help");
		 helpmenu.setMenu(help);
		 
		
		 create_account=new MenuItem(master,SWT.None);
		 create_account.setText("Create/Edit &Account");
		 create_account.setAccelerator(SWT.F2);
		

		 edit_org=new MenuItem(master,SWT.None);
		 edit_org.setText("Edit Organization Details");
		 edit_org.setAccelerator(SWT.CTRL + 'e');
		 if(StartupController.getClosedBooksStatus())
		 {
			 edit_org.setEnabled(false);
		 }

		 add_proj=new MenuItem(master,SWT.None);
		 add_proj.setText("&Project");
		 add_proj.setAccelerator(SWT.CTRL + 'p');
		 bank_rec_statement=new MenuItem(master,SWT.None);
		 bank_rec_statement.setText("&Bank Reconciliation Statement");
		 bank_rec_statement.setAccelerator(SWT.F12);
		 
		 Contra=new MenuItem(transactions,SWT.None);
		 Contra.setText("&Contra");
		 Contra.setAccelerator(SWT.F4);
		 Payment=new MenuItem(transactions,SWT.None);
		 Payment.setText("&Payment");
		 Payment.setAccelerator(SWT.F5);
		 Receipt=new MenuItem(transactions,SWT.None);
		 Receipt.setText("&Receipt");
		 Receipt.setAccelerator(SWT.F6);
		 Journal=new MenuItem(transactions,SWT.None);
		 Journal.setText("&Journal");
		 Journal.setAccelerator(SWT.F7);
		 Sales=new MenuItem(transactions,SWT.None);
		 Sales.setText("&Sales");
		 Sales.setAccelerator(SWT.F8);
		 Purchase=new MenuItem(transactions,SWT.None);
		 Purchase.setText("P&urchase");
		 Purchase.setAccelerator(SWT.F9);
		 Credit_Note=new MenuItem(transactions,SWT.None);
		 Credit_Note.setText("Cre&dit Note");
		 Credit_Note.setAccelerator(SWT.CTRL + SWT.F2);
		 Debit_Note=new MenuItem(transactions,SWT.None);
		 Debit_Note.setText("De&bit Note");
		 Debit_Note.setAccelerator(SWT.CTRL + SWT.F3);
		 
		 Sales_Return=new MenuItem(transactions,SWT.None);
		 Sales_Return.setText("&Sales Return");
		 Sales_Return.setAccelerator(SWT.CTRL + SWT.F4);
		 
		 Purchase_Return=new MenuItem(transactions,SWT.None);
		 Purchase_Return.setText("Purc&hase Return");
		 Purchase_Return.setAccelerator(SWT.CTRL + SWT.F5);
		 
		 FindVoucher=new MenuItem(transactions, SWT.None);
		 FindVoucher.setText("&Find Voucher");
		 FindVoucher.setAccelerator(SWT.CTRL + 'F');
		                                    
		 Ledger=new MenuItem(reports,SWT.None);
		 Ledger.setText("&Ledger");
		 Ledger.setAccelerator(SWT.F3);
		 Trial_Balance=new MenuItem(reports,SWT.None);
		 Trial_Balance.setText("&Trial Balance");
		 Trial_Balance.setAccelerator(SWT.CTRL + SWT.F7);
		 Project_Statement=new MenuItem(reports,SWT.None);
		 Project_Statement.setText("Pr&oject Statement");
		 Project_Statement.setAccelerator(SWT.CTRL + SWT.F11);
		 Cash_Flow=new MenuItem(reports,SWT.None);
		
		 Cash_Flow.setAccelerator(SWT.CTRL + SWT.F10);
		 Balance_Sheet=new MenuItem(reports,SWT.None);
		 if(globals.session[4].equals("Profit Making"))
		 {
			 Balance_Sheet.setText("Bala&nce Sheet"); 
			 Cash_Flow.setText("Cash &Flow");
		 }
		 if(globals.session[4].equals("NGO"))
			{
				Balance_Sheet.setText("Statement of &Affairs");
				Cash_Flow.setText("Receipt && Payment");
			}
		 Balance_Sheet.setAccelerator(SWT.CTRL + SWT.F9);
		 
		 Profit_and_Loss_Account=new MenuItem(reports,SWT.None);
		 
		 if(globals.session[4].equals("Profit Making"))
		{
			Profit_and_Loss_Account.setText("Prof&it and Loss Account");
		}
		if(globals.session[4].equals("NGO"))
		{
			Profit_and_Loss_Account.setText("&Income && Expenditure Account");
		}
		Profit_and_Loss_Account.setAccelerator(SWT.CTRL+SWT.F8);
		
		 Account_List=new MenuItem(reports, SWT.None);
		 Account_List.setText("&List Of Accounts");
		 Account_List.setAccelerator(SWT.CTRL + 'l');
		 
		
		
		 GNUKhata_Manual=new MenuItem(help,SWT.None);
		 GNUKhata_Manual.setText("GNU&Khata Manual ");
		 Authors=new MenuItem(help,SWT.None);
		 Authors.setText("&Authors");
		 GNUKhata_License=new MenuItem(help,SWT.None);
		 GNUKhata_License.setText("GNUKhata &License");
		 About_GNUKhata=new MenuItem(help,SWT.None);
		 About_GNUKhata.setText("A&bout GNUKhata ");
		
		 Change_User = new MenuItem(sessionbtn,SWT.None);
		 Change_User.setText("Lo&gout");
		 Change_User.setAccelerator(SWT.CTRL+'g');
		 ChangeOrg=new MenuItem(sessionbtn,SWT.None);
		 ChangeOrg.setText("C&hange Organisation");
		 ChangeOrg.setAccelerator(SWT.CTRL+'h');
		 Quit=new MenuItem(sessionbtn,SWT.None);
		 Quit.setText("&Quit");
		 Quit.setAccelerator(SWT.CTRL+'q');
		 
		
		    this.setMenuBar(menubar);
		    this.setBackgroundImage(globals.backImg);
			this.setBackgroundMode(SWT.INHERIT_FORCE);

tb = new ToolBar(menubar.getShell(),SWT.BORDER| SWT.VERTICAL|SWT.WRAP);
//tb.setLocation(0,menubar.getShell().getClientArea().width-15  );
tb.setFont(new Font(display,"Times New Roman", 10, SWT.ITALIC|SWT.BOLD ) );
FormData fd = new FormData();
fd.top = new FormAttachment(0);
fd.left = new FormAttachment(80);
fd.right = new FormAttachment(100);			
tb.setLayoutData(fd);
Color clrWhite = Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
Color clrBlack = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
Color lightBlue = new Color(this.getDisplay(),198,232,249);


tb.setBackground(lightBlue);
tb.setForeground(clrBlack);


tiAccount = new ToolItem(tb , SWT.PUSH);
tiAccount.setText("Create/Edit Account: f2");
//tiAccount.setFont(new Font(display,"Times New Roman",14,clr));
tiAccount.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		create_account.notifyListeners(SWT.Selection, new Event());
	}
});


tiAccountReport = new ToolItem(tb, SWT.PUSH);
tiAccountReport.setText("List Of Accounts : CTRL + L");
tiAccountReport.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		Account_List.notifyListeners(SWT.Selection, new Event());
	}
});

 tiLedger = new ToolItem(tb, SWT.PUSH);
tiLedger.setText("Ledger: f3");		

tiLedger.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Ledger.notifyListeners(SWT.Selection, new Event());
	}
});
			
 tiContra = new ToolItem(tb, SWT.PUSH);
tiContra.setText("Contra: f4");
tiContra.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Contra.notifyListeners(SWT.Selection, new Event());
	}
});

 tiPayment = new ToolItem(tb, SWT.PUSH);
tiPayment.setText("Payment: f5");
tiPayment.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Payment.notifyListeners(SWT.Selection, new Event());
	}
});

 tiReceipt  = new ToolItem(tb, SWT.PUSH);
tiReceipt.setText("Receipt: f6");
tiReceipt.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Receipt.notifyListeners(SWT.Selection, new Event());
	}
});
 tiJournal = new ToolItem(tb, SWT.PUSH);
tiJournal.setText("Journal: f7");		
tiJournal.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Journal.notifyListeners(SWT.Selection, new Event());
	}
});

 tiSales = new ToolItem(tb, SWT.PUSH);
tiSales.setText("Sales: f8");
tiSales.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Sales.notifyListeners(SWT.Selection, new Event());
	}
});

 tiPurchase = new ToolItem(tb, SWT.PUSH);
tiPurchase.setText("Purchase: f9");
tiPurchase.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Purchase.notifyListeners(SWT.Selection, new Event());
	}
});

 tiCredit = new ToolItem(tb, SWT.PUSH );
tiCredit.setText("Credit Note: CTRL+f2");
tiCredit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Credit_Note.notifyListeners(SWT.Selection, new Event());
	}
});

 tiDebit = new ToolItem(tb, SWT.PUSH);
tiDebit.setText("Debit Note: CTRL+f3");
tiDebit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Debit_Note.notifyListeners(SWT.Selection, new Event());
	}
});


 tiSalesReturn = new ToolItem(tb, SWT.PUSH);
tiSalesReturn.setText("Sales Return: CTRL+f4");
tiSalesReturn.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Sales_Return.notifyListeners(SWT.Selection, new Event());
	}
});

 tiPurchaseReturn = new ToolItem(tb, SWT.PUSH);
tiPurchaseReturn.setText("Purchase Return: CTRL+f5");
tiPurchaseReturn.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Purchase_Return.notifyListeners(SWT.Selection, new Event());
	}
});

if(StartupController.getClosedBooksStatus())
{
	 tiContra.setEnabled(false);
	 tiPayment.setEnabled(false);
	 tiReceipt.setEnabled(false);
	 tiSales.setEnabled(false);
	 tiSalesReturn.setEnabled(false);
	 tiPurchase.setEnabled(false);
	 tiPurchaseReturn.setEnabled(false);
	 tiJournal.setEnabled(false);
	 tiDebit.setEnabled(false);
	 tiCredit.setEnabled(false);
	tiAccount.setEnabled(false);
}
        

 tiTrialBalance = new ToolItem(tb, SWT.PUSH);
tiTrialBalance.setText("Trial Balance: CTRL+f7");
tiTrialBalance.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Trial_Balance.notifyListeners(SWT.Selection, new Event());
	}
});
 tiPnl = new ToolItem(tb, SWT.PUSH);
if(globals.session[4].equals("Profit Making"))
{
	tiPnl.setText("Profit And Loss: CTRL+f8");
}
if(globals.session[4].equals("NGO"))
	{
	tiPnl.setText("Income&&Expenditure: CTRL+f8");
	}


tiPnl.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Profit_and_Loss_Account.notifyListeners(SWT.Selection, new Event());
	}
});


 tiBalanceSheet = new ToolItem(tb, SWT.PUSH);
if(globals.session[4].equals("Profit Making"))
{
	tiBalanceSheet.setText("Balance Sheet :  CTRL+f9");
}
if(globals.session[4].equals("NGO"))
	{
	tiBalanceSheet.setText("Statement of &Affairs :  CTRL+f9");
	}

		

tiBalanceSheet.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Balance_Sheet.notifyListeners(SWT.Selection, new Event());
	}
});
 tiCashFlow = new ToolItem(tb, SWT.PUSH);
 if(globals.session[4].equals("Profit Making"))
 {
	 tiCashFlow.setText("Cash Flow: CTRL+f10");
 }
 if(globals.session[4].equals("NGO"))
 	{
	 tiCashFlow.setText("Receipt && Payment: CTRL+f10");
 	}

tiCashFlow.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Cash_Flow.notifyListeners(SWT.Selection, new Event());
	}
});


 tiProjectStatement = new ToolItem(tb, SWT.PUSH);
tiProjectStatement.setText("Project Statement: CTRL+f11");
tiProjectStatement.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		Project_Statement.notifyListeners(SWT.Selection, new Event());
	}
});

 tiBankReco = new ToolItem(tb, SWT.PUSH);
tiBankReco.setText("Bank Reconciliation Statement: f12");
tiBankReco.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
//super.widgetSelected(arg0);
		bank_rec_statement.notifyListeners(SWT.Selection, new Event());
	}
});

 tiEditOrg=new ToolItem(tb, SWT.PUSH);
tiEditOrg.setText("Edit Organisation : CTRL+E");
tiEditOrg.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		edit_org.notifyListeners(SWT.Selection, new Event());
	}
});


if(StartupController.getClosedBooksStatus())
{
	 tiEditOrg.setEnabled(false);
}
 tiAddProject=new ToolItem(tb, SWT.PUSH);
tiAddProject.setText("Project: CTRL+P");
tiAddProject.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		add_proj.notifyListeners(SWT.Selection, new Event());
	}
});


 tiFindVoucher=new ToolItem(tb, SWT.PUSH);
tiFindVoucher.setText("Find Voucher: CTRL+F");
tiFindVoucher.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		//super.widgetSelected(arg0);
		FindVoucher.notifyListeners(SWT.Selection, new Event());
	}
});

tiLogout = new ToolItem(tb, SWT.PUSH);
tiLogout.setText("Logout: CTRL+G");
tiLogout.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Change_User.notifyListeners(SWT.Selection, new Event());
	}
});

tiChange_org = new ToolItem(tb, SWT.PUSH);
tiChange_org.setText("Change Organisation: CTRL+H");
tiChange_org.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		ChangeOrg.notifyListeners(SWT.Selection, new Event());
	}
});


 tiQuit= new ToolItem(tb, SWT.PUSH);
tiQuit.setText("Quit: CTRL+Q");
tiQuit.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Quit.notifyListeners(SWT.Selection, new Event());
	}
});

/*ToolItem NewUser= new ToolItem(tb, SWT.PUSH);
NewUser.setText("New User: CTRL+U");
NewUser.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		New_User.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem ChangePass= new ToolItem(tb, SWT.PUSH);
ChangePass.setText("Change Password: CTRL+W");
ChangePass.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Change_Password.notifyListeners(SWT.Selection, new Event());
	}
});

ToolItem Deleteorg= new ToolItem(tb, SWT.PUSH);
Deleteorg.setText("Delete Organisation: CTRL+D");
Deleteorg.addSelectionListener(new SelectionAdapter() {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		Delete_Organisation.notifyListeners(SWT.Selection, new Event());
	}
});
*/
tb.setVisible(true);
tb.pack();
if (globals.session[7].toString().equals("-1"))
{
	 New_User=new MenuItem(admin,SWT.None);
	 New_User.setText("New &User");
	 New_User.setAccelerator(SWT.CTRL+'u');
	 Remove_User=new MenuItem(admin,SWT.None);
	 Remove_User.setText("&Remove User");
	 Remove_User.setAccelerator(SWT.CTRL+'r');
	 Change_Password=new MenuItem(admin,SWT.None);
	 Change_Password.setText("Change Pass&word");
	 Change_Password.setAccelerator(SWT.CTRL+'w');
	 Roll_Over=new MenuItem(admin,SWT.None);
	 Roll_Over.setText("Cl&ose Books/Roll Over");
	 Roll_Over.setAccelerator(SWT.CTRL+'o');
	 Delete_Organisation=new MenuItem(admin,SWT.None);
	 Delete_Organisation.setText("&Delete Organisation");
	 Delete_Organisation.setAccelerator(SWT.CTRL+'d');
}	

if (globals.session[7].toString().equals("0"))
{
	 New_User=new MenuItem(admin,SWT.None);
	 New_User.setText("New &User");
	 New_User.setAccelerator(SWT.CTRL+'u');
	 Change_Password=new MenuItem(admin,SWT.None);
	 Change_Password.setText("Change Pass&word");
	 Change_Password.setAccelerator(SWT.CTRL+'w');
}
if (globals.session[7].toString().equals("1"))
{
	 
	 Change_Password=new MenuItem(admin,SWT.None);
	 Change_Password.setText("Change Pass&word");
	 Change_Password.setAccelerator(SWT.CTRL+'w');
	 tiTrialBalance.setEnabled(false);
	 tiBalanceSheet.setEnabled(false);
	 tiPnl.setEnabled(false);
	 tiEditOrg.setEnabled(false);
	 Trial_Balance.setEnabled(false);
	 Profit_and_Loss_Account.setEnabled(false);
	 Balance_Sheet.setEnabled(false);
	 edit_org.setEnabled(false);
}
	if(StartupController.getRolloverStatus())
	{
		Roll_Over.setEnabled(false);
	}
	if(StartupController.getClosedBooksStatus())
	 {
		 Contra.setEnabled(false);
		 Payment.setEnabled(false);
		 Receipt.setEnabled(false);
		 Sales.setEnabled(false);
		 Sales_Return.setEnabled(false);
		 Purchase.setEnabled(false);
		 Purchase_Return.setEnabled(false);
		 Journal.setEnabled(false);
		 Debit_Note.setEnabled(false);
		 Credit_Note.setEnabled(false);
		create_account.setEnabled(false);
		create_account.setEnabled(false);
		New_User.setEnabled(false);
		tiAddProject.setEnabled(false);
		add_proj.setEnabled(false);
		bank_rec_statement.setEnabled(false);
		tiBankReco.setEnabled(false);
		 
	 }
	

this.pack();

			
			/*MessageBox mswidth = new MessageBox(new Shell(),SWT.OK);
			mswidth.setMessage("now the width of main shell is " + Integer.toString(this.getClientArea().width ));
			mswidth.open();*/
			formArea  = new Composite(this, SWT.NONE);
			fd = new FormData();
			fd.top = new FormAttachment(1);
			fd.left= new FormAttachment(0);
			fd.bottom = new FormAttachment(100);
			fd.right = new FormAttachment(100);
			formArea.setLayoutData(fd);
			formArea.setBounds(bounds);
			
			//formArea.pack();
			/*MessageBox wval = new MessageBox(new Shell(),SWT.OK);
			wval.setMessage("with of form area is " + Integer.toString(formArea.getClientArea().width ));
			wval.open();*/
			
			
			
			Group grpMain = new Group(formArea, SWT.NONE);
			layout = new FormData();
			//grpMain.setText("Note");
			layout.right=new FormAttachment(60);
			grpMain.setLocation(90, 100);//left,top
			grpMain.setBackground(clrWhite);
			
			
			Label lblnote2 = new Label(grpMain, SWT.LEFT);
			lblnote2.setFont(new Font(display, "Times New Roman", 16, SWT.ITALIC));
			lblnote2.setText("GNUKhata creates the following accounts on its own.\n  1. Closing Stock under Group Current Assets, Sub-Group Inventory\n  2. Opening Stock under Group Direct Expense\n  3. Stock at the Beginning under Group Current Assets, Sub-Group Inventory\n  4. Profit && Loss Or Income && Expenditure under Group Direct Income");
			lblnote2.setLocation(10, 35);
			lblnote2.pack();
			//grpMain.pack();
		        
			
			Label lblnote3 = new Label(grpMain, SWT.NONE);
			lblnote3.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
			lblnote3.setText("\n\n\n\n\t"+" "+" "+"On any screen press f1 to view Toolbar.");
			lblnote3.setLocation(20, 65);
			lblnote3.pack();
			grpMain.pack();

			
			if(StartupController.getClosedBooksStatus())
			 {
					transactionsmenu.setEnabled(false);
					tiFindVoucher.setEnabled(false);

			 }
			 else
			{
				 transactionsmenu.setText("&Transactions");
				 transactionsmenu.setMenu(transactions);


				 
			 }

			
			
			/*lblnote = new Label(grpMain, SWT.CENTER);
			lblnote.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
			lblnote.setText("\tPress and Hold alt for screen specific shortcuts.");
			lblnote.setLocation(20,20);
			lblnote.pack();   
			*/
			
			
			/*if (StartupController.getClosedBooksStatus())
			{
				tiPnl.setEnabled(false);
				Profit_and_Loss_Account.setEnabled(false);
			}*/
			
		    this.setImage(globals.icon);
		    this.makeaccssible(this);
		    this.setEvent();
		    this.pack();
		    this.open();		   
		    this.showView();
	}
	
	protected void showprogress() {

		lblDelOrgMsg.setVisible(true);
    	selectbar.setVisible(true);
    	MainShell.lblLine.setVisible(true);
    	MainShell.lblLogo.setVisible(true);
    	MainShell.lblOrgDetails.setVisible(true);
    	selectbar.setMaximum(100000);
		for(int i=0;i<selectbar.getMaximum();i=i+20)
        
        {
			selectbar.setSelection(i);
        }
      }

	
	private void setEvent()
	{
		
		this.addListener(SWT.Close, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setText("Confirm?");
				//CustomDialog msg =new CustomDialog(new Shell());
				msg.setMessage("Are you sure?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					menubar.getShell().dispose();
					System.exit(0);
				}	
				if(answer == SWT.NO)
				{
					arg0.doit=false;
				}
			}
		});
		
		this.getDisplay().addFilter(SWT.KeyDown, new Listener() {
			
			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.F1 )					
				{
						tb.setVisible(true);
						tb.setFocus();
						tb.traverse(2);
						
					
				}
					else
					{
						try {
							if(arg0.keyCode!= SWT.ARROW_DOWN && arg0.keyCode!= SWT.ARROW_UP && arg0.keyCode!= SWT.SPACE && arg0.keyCode!= SWT.PAGE_DOWN && arg0.keyCode!= SWT.PAGE_UP)
								{
								tb.setVisible(false);
								}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					}
				
				
			}
		});
		formArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				//make the toolbar visible.
			}
			});
		
		
		
			create_account.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent se){
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if(ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				
			AccountTabForm tb = new AccountTabForm(formArea,SWT.NONE);
			tb.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
		
			}
	   });
			
		edit_org.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				EditOrganisation eo=new EditOrganisation(formArea, SWT.None);
				eo.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				eo.txtRegiNo.setFocus();
					
			}
		});
		
			
		add_proj.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				gnukhata.controllers.StartupController.getProjectList(formArea);
				}
			
		});
		
		Contra.addSelectionListener(new SelectionAdapter() 
			{
				public void widgetSelected(SelectionEvent se)
				{
					tb.setVisible(false);
					Control[] ctrls = formArea.getChildren();
					
					if (ctrls.length > 0)
					{
						ctrls[0].dispose();
					}
					AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Contra");
					addnewvoucher.txtvoucherno.setFocus();
					addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				}
		
		});
		Journal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
			
				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Journal");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		Payment.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Payment");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		Receipt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Receipt");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		Credit_Note.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Credit Note");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		Debit_Note.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Debit Note");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		Sales.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Sales");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);

			}
		});
		Sales_Return.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Sales Return");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		Purchase.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Purchase");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		Purchase_Return.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}			
				AddNewVoucherComposite addnewvoucher = new AddNewVoucherComposite(formArea, SWT.NONE,"Purchase Return");
				addnewvoucher.txtvoucherno.setFocus();
				addnewvoucher.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
			}
		});
		
		FindVoucher.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if(ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				/*VoucherTabForm.typeFlag = "Find Voucher";
				VoucherTabForm vtb = new VoucherTabForm(formArea, SWT.None);*/
				
				String[] search_values = null;
				FindandEditVoucherComposite fdvoucher = new FindandEditVoucherComposite(formArea, SWT.NONE,true, false, 0,search_values);
				fdvoucher.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
				
				}
		});
		
		bank_rec_statement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewReconciliation vr = new viewReconciliation(formArea,SWT.NONE);
				vr.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		GNUKhata_License.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewLicense vl = new viewLicense(formArea,SWT.NONE);
				vl.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		Authors.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewauthors va = new viewauthors(formArea,SWT.NONE);
				va.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		About_GNUKhata.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				final String COPYRIGHT  = "\u00a9";
				MessageBox msg = new MessageBox(new Shell(), SWT.OK |SWT.ICON_INFORMATION);
				msg.setText("Information!");
				msg.setMessage("GNUKhata Version 1.6.1 \n"
						+ COPYRIGHT+"Copyright 2007-12 Commet Media Foundation\n"
						+ COPYRIGHT+"Copyright 2013-14 Digital Freedom Foundation");
				msg.open();
			}
		});
		
		
		Trial_Balance.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewTrialBalance vtb = new viewTrialBalance(formArea,SWT.NONE);
				vtb.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		Project_Statement.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				ViewProjectStatement vps = new ViewProjectStatement(formArea, SWT.NONE);
				vps.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
if(globals.session[7].toString().equals("-1") || globals.session[7].toString().equals("0"))
{
	
		New_User.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				createNewUser cnu= new createNewUser(formArea, SWT.NONE);
				cnu.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		}
		
		Profit_and_Loss_Account.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				/*String accountname = null;
				String strFromdt =  globals.session[2].toString();
				String strTodt =  globals.session[3].toString();
				String startDate = strFromdt.substring(6)+"-"+strFromdt.substring(3,5)+"-"+strFromdt.substring(0,2);
				String endDate = strTodt.substring(6)+"-"+strTodt.substring(3,5)+"-"+strTodt.substring(0,2);
				
				if(globals.session[4].equals("Profit Making"))
				 {
					 accountname = "Profit & Loss";  
				 }
				if(globals.session[4].equals("NGO"))
				 {
					 accountname = "Income & Expenditure";  
				 }*/
					
				
				
					viewProfitAndLoss vpl = new viewProfitAndLoss(formArea,SWT.NONE);
					vpl.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
				
				
			}
		});
		
		
		Balance_Sheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewBalanceSheet vbs= new viewBalanceSheet(formArea, SWT.NONE);
				vbs.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});


		Cash_Flow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}
				viewCashflow vcf = new viewCashflow(formArea,SWT.NONE);
				vcf.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
				
			}
		});
		
		Account_List.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			tb.setVisible(false);
			Control[] ctrls = formArea.getChildren();
			if (ctrls.length > 0)
			{
				ctrls[0].dispose();
			}
			
			gnukhata.controllers.reportController.getAccountReport(formArea);
		
		}
	});
		
		Ledger.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				ViewLedger vl=new ViewLedger(formArea,SWT.NONE,"","","","",false,false,false,"","",false);
								vl.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
			}
		});
		
		
		Change_User.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setText("Confirm?");
				//CustomDialog msg =new CustomDialog(new Shell());
				msg.setMessage("Do you wish to log out?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					
					//menubar.getShell().dispose();
					String[] connectParams = new String[] { globals.session[1].toString(),globals.session[2].toString(),globals.session[3].toString(),globals.session[4].toString()};
					String id = gnukhata.controllers.StartupController.getConnection(connectParams);
					/*
					 * MessageBox success = new MessageBox(new Shell(), SWT.OK);
					 * success.setText("success"); success.setMessage("id is " +
					 * id); success.open();
					 */	
					tb.dispose();
					menubar.getShell().getDisplay().dispose();
					gnukhata.controllers.StartupController.showLoginForm();
					
					//System.gc();
					
					
					
				}
			}
		});
		
		
		
		ChangeOrg.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setText("Confirm?");
				//CustomDialog msg =new CustomDialog(new Shell());
				msg.setMessage("Do you wish to change Organisation ?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					tb.dispose();
					menubar.getShell().getDisplay().dispose();
					//menubar.getShell().dispose();
					System.gc();
					startupForm sf = new startupForm();
				}
			}
		});
		
		Quit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setText("Confirm?");
				//CustomDialog msg =new CustomDialog(new Shell());
				msg.setMessage("Are you sure?");
				int answer = msg.open();
				if(answer == SWT.YES)
				{
					menubar.getShell().dispose();
					System.exit(0);
				}				
			}
		});
		
		if(globals.session[7].toString().equals("-1"))
		{
			Remove_User.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					tb.setVisible(false);
					Control[] ctrls = formArea.getChildren();
					if (ctrls.length > 0)
					{
						ctrls[0].dispose();
					}
					RemoveUser ru= new RemoveUser(formArea, SWT.NONE);
					ru.setSize(formArea.getClientArea().width, formArea.getClientArea().height);
					
				}
			});
			
			
			Delete_Organisation.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				MessageBox msg = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES | SWT.NO);
				msg.setText("Confirm?");
				//CustomDialog msg =new CustomDialog(new Shell());
				msg.setMessage("Are you sure you want to delete this Organisation?");
				int answer = msg.open();
				if(answer == SWT.NO)
				{
					return;
				}
				if(answer==SWT.YES)
				{	
					formArea.dispose();
					showprogress();
					tb.dispose();
					menubar.getShell().dispose();
					System.gc();
					gnukhata.controllers.StartupController.DeleteOrg(grandParent, strOrgName, strFromYear, strToYear);
					//dispose();
					gnukhata.controllers.StartupController.showstartupForm();
					
				}
			}
		});
		
		Roll_Over.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				RolloverandClosebooks rc = new RolloverandClosebooks(formArea, SWT.NONE);
				rc.setSize(formArea.getClientArea().width,formArea.getClientArea().height);
			}
		});
		}
		if(globals.session[7].toString().equals("-1") || globals.session[7].toString().equals("0") || globals.session[7].toString().equals("1"))
		{
		
		Change_Password.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				tb.setVisible(false);
				Control[] ctrls = formArea.getChildren();
				if (ctrls.length > 0)
				{
					ctrls[0].dispose();
				}

				ChangePassword cp=new ChangePassword(formArea, SWT.NONE);
				cp.setSize(formArea.getClientArea().width, formArea.getClientArea().height);		
				
			}
		});
		}
	
		}

	
	
	
	 protected void checkSubclass()
	 	{
	        // Disable the check that prevents subclassing of SWT components
	    }
	
	 public void makeaccssible(Control c)
		{
			c.getAccessible();
		}
	 
	 private void showView()
		{
			try {
				while(! this.isDisposed())
				{
					if(! this.getDisplay().readAndDispatch())
					{
						this.getDisplay().sleep();
						if ( ! this.getMaximized())
						{
							this.setMaximized(true);
						}
					}				
				}
				this.dispose();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					while(! this.isDisposed())
					{
						if(! this.getDisplay().readAndDispatch())
						{
							this.getDisplay().sleep();
							if ( ! this.getMaximized())
							{
								this.setMaximized(true);
							}
						}				
					}
					this.dispose();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}


		}
	 
	 // TODO kindly remove the main() after testing the user interface.
	public static void main(String[] args) 
	{
		Display display=Display.getDefault();
		final MainShell mainscr =new MainShell(display);
		mainscr.pack();
		mainscr.open();
		
		while (!mainscr.isDisposed() ) {
			
		if (!display.readAndDispatch())
		{
			 display.sleep();
			 
		}
		
	}
		
	}
	
}
