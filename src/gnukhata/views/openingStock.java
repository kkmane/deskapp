package gnukhata.views;
import gnukhata.globals;
import gnukhata.controllers.accountController;
import gnukhata.controllers.reportmodels.AddMultipleAcc;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.GroupLayout.Group;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
 
public class openingStock extends Dialog {
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
Group grp;
  String value;
  String Grpname;
  String SubGrpName;
  String accname;
  Double openingbalance;
  String suggestedAccountCode = "null";
  String TxtOpeningBal;
  String accname2;
  Object[] queryParams = new Object[8];
  public static String newAccount;
  public static Boolean cancelflag = false;
  public static Boolean saveflag = false;
  boolean DecimalFlag = false;
  boolean accountExists = false;
  String searchText = "";
  long searchTexttimeout = 0;
  ArrayList<AddMultipleAcc>  lstVoucherRows;

 
  /**
   * @param parent
 * @param subgrpname 
 * @param grpname 
   */
  public openingStock(Shell parent) {
    super(parent);
    Grpname="Direct Expense";
    SubGrpName="No Sub-Group";
  }
 
  /**
   * @param parent
   * @param style
   */
 
 
  
  
/**
   * Makes the dialog visible.
   *
   * @return
   */
  
  public String open()
	{
	 
	  	Shell parent = getParent();
	 	final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
		shell.setText("Add Opening Stock");
		final int shellwidth = shell.getClientArea().width;
		final int finshellwidth = shellwidth-(shellwidth*10/100);
		final int shellheight = shell.getClientArea().height;
		final int finshellheight = shellheight-(shellheight*10/100);
		shell.setSize(finshellwidth, finshellheight);
	//	final int insetX = 4, insetY = 4;
		FormLayout formlayout = new FormLayout();
		shell.setLayout(formlayout);
      
		
      final Label lblgroup = new Label(shell, SWT.NULL);
      lblgroup.setText("&Group Name:");
      FormData layout=new FormData();
      layout.top= new FormAttachment(4);
      //layout.bottom= new FormAttachment(16);
      layout.left= new FormAttachment(2);
      //layout.right= new FormAttachment(25);
      lblgroup.setLayoutData(layout);
      lblgroup.setVisible(true);
     
      
      final Text txtgroup = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);      
      layout=new FormData();
      txtgroup.setText(Grpname);
      layout.top= new FormAttachment(3);
      layout.left= new FormAttachment(lblgroup,5);
      layout.right= new FormAttachment(44);
      txtgroup.setLayoutData(layout);
      queryParams[0] = Grpname;
      txtgroup.setEditable(false);
      
      
      
      final Label lblsubgroup = new Label(shell, SWT.NULL);
      lblsubgroup.setText("Sub-Group Name:");
      layout=new FormData();
      layout.top= new FormAttachment(4);
      //layout.bottom= new FormAttachment(16);
      layout.left= new FormAttachment(46);
      //layout.right= new FormAttachment(25);
      lblsubgroup.setLayoutData(layout);
           
      final Text txtsubgroup = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
      txtsubgroup.setText(SubGrpName);
      layout=new FormData();
      layout.top= new FormAttachment(3);
      layout.left= new FormAttachment(lblsubgroup,5);
      layout.right= new FormAttachment(98);
      txtsubgroup.setLayoutData(layout);
      queryParams[1]=SubGrpName;
      queryParams[2]="";
      txtsubgroup.setEditable(false);
      
            
      
      
       
		
		final Label lblaccountname = new Label(shell, SWT.NULL);
	      lblaccountname.setText("Account Name");
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      //layout.bottom= new FormAttachment(16);
	      layout.left= new FormAttachment(3);
	      //layout.right= new FormAttachment(25);
	      lblaccountname.setLayoutData(layout);
	      	      
	      final Text txtaccountname = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
	      txtaccountname.setText("Opening Stock");
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      layout.left= new FormAttachment(lblaccountname,5);
	      layout.right= new FormAttachment(50);
	      txtaccountname.setLayoutData(layout);
	      txtaccountname.setEditable(false);

	      final Label lblopeningbal = new Label(shell, SWT.NULL);
	      lblopeningbal.setText("Opening Balance");
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      //layout.bottom= new FormAttachment(16);
	      layout.left= new FormAttachment(52);
	      layout.right= new FormAttachment(73);
	      lblopeningbal.setLayoutData(layout);
	           
	    
	      
	      final Text txtopeningbal = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER|SWT.RIGHT);
	      layout=new FormData();
	      layout.top= new FormAttachment(73);
	      layout.left= new FormAttachment(73);
	      layout.right= new FormAttachment(95);
	      txtopeningbal.setLayoutData(layout);
	      txtopeningbal.setText("0.00");
	      txtopeningbal.setFocus();

		  
	    	
      final Button btnSave = new Button(shell, SWT.NONE);
      btnSave.setText("&Save");
      layout = new FormData();
      layout.top= new FormAttachment(89);
      layout.left= new FormAttachment(25);
      btnSave.setLayoutData(layout);
      
      final Button btnCancel = new Button(shell, SWT.NONE);
      btnCancel.setText("&Cancel");
      layout = new FormData();
      layout.top= new FormAttachment(89);
      layout.left= new FormAttachment(65);
      btnCancel.setLayoutData(layout);
      
     
      
      Background =  new Color(this.getDisplay() ,220 , 224, 227);
      Foreground = new Color(this.getDisplay() ,0, 0,0 );
      FocusBackground  = new Color(this.getDisplay(),78,97,114 );
      FocusForeground = new Color(this.getDisplay(),255,255,255);
      BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

      txtaccountname.setFocus();
      txtaccountname.setBackground(FocusBackground);
      txtaccountname.setForeground(FocusForeground);
      
     
  

txtopeningbal.addFocusListener(new FocusAdapter() {
	
	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		txtopeningbal.setBackground(Background);
		txtopeningbal.setForeground(Foreground);
		
	}
});
      txtopeningbal.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	            case SWT.TAB:
	            case SWT.CR:
	            case SWT.KEYPAD_CR:
	            case SWT.KEYPAD_SUBTRACT:
	            
	                return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == false)
				{
					DecimalFlag = true;
					return;
				}
				if(arg0.keyCode == SWT.KEYPAD_DECIMAL&& DecimalFlag == true)
				{
					arg0.doit= false;
					return;
				}
				
		if(arg0.keyCode == 46)
		{
			return;
		}
		
		if(arg0.keyCode == 62)
		{
			  arg0.doit = false;
			
		}
		if(arg0.keyCode == 45)
		{
			return;
		}
	
	        if (!Character.isDigit(arg0.character)) {
	            arg0.doit = false;  // disallow the action
	        }
	        

				
			}
		});
      
     
    
      
  	txtopeningbal.addTraverseListener(new TraverseListener() {
		
		@Override
		public void keyTraversed(TraverseEvent arg0) {
			// TODO Auto-generated method stub
			
							if(arg0.detail == SWT.TRAVERSE_RETURN)
							{

								
								
								
								if( txtopeningbal.getText().trim().startsWith(".") || txtopeningbal.getText().trim().endsWith(".") || txtopeningbal.getText().trim().endsWith("-")  )
								{
									MessageBox msgValueError = new MessageBox(new Shell(), SWT.OK | SWT.ICON_ERROR);
									msgValueError.setText("Error!");
									msgValueError.setMessage("Please enter amount");
									msgValueError.open();
									Display.getCurrent().asyncExec(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											txtopeningbal.setFocus();
											txtopeningbal.setText("");
										}
									});      
									
									arg0.doit = false;
								
									return;
										}
								if(txtopeningbal.getText().equals(""))
								{
								txtopeningbal.setText("0.00");	
								}
							
							
							if(arg0.detail==SWT.TRAVERSE_TAB_NEXT )
							{
								btnSave.setFocus();
							}
							
				arg0.doit = false;
				
				
					
				
				
		}
		}
	});
  	
  btnSave.addFocusListener(new FocusAdapter() {
	  @Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		  btnSave.setBackground(FocusBackground);
		  btnSave.setForeground(BtnFocusForeground);
			
	}
	  @Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		  btnSave.setBackground(Background);
		  btnSave.setForeground(Foreground);
			
	  }
});
  btnCancel.addFocusListener(new FocusAdapter() {
	  @Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusGained(arg0);
		  btnCancel.setBackground(FocusBackground);
		  btnCancel.setForeground(BtnFocusForeground);
			
	}
	  @Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
		//super.focusLost(arg0);
		  btnCancel.setBackground(Background);
		  btnCancel.setForeground(Foreground);
			
	  }
});

  	btnSave.addSelectionListener(new SelectionAdapter() {
  		@Override
  		public void widgetSelected(SelectionEvent arg0) {
  			// TODO Auto-generated method stub
  			//super.widgetSelected(arg0);
  			String result1 = accountController.accountExists(txtaccountname.getText().trim());
			if (Integer.valueOf(result1) == 1)
			{
				MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR | SWT.ICON_WARNING);
				msg.setText("Warning!");
				msg.setMessage("The account name you entered already exists, please choose another name.");
				msg.open();
				
				Display.getCurrent().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						txtaccountname.setText("");
						txtaccountname.setFocus();
						
					}
				});
				return;
			
			}
			accname = txtaccountname.getText().trim();
  			queryParams[3]=accname;
  			openingbalance=Double.valueOf(txtopeningbal.getText().trim());
  			System.out.println("accountname " +accname + "openingbalaec" +openingbalance);
  				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);
				nf.setGroupingUsed(false);

				queryParams[5] = nf.format(openingbalance);
				queryParams[6] = nf.format(openingbalance);
			
  			
  			boolean result = false;
  			queryParams[4] = globals.session[5];
  			queryParams[7]="";
  			accountController.setAccount(queryParams);
  			
  			
  		
  		

			saveflag = true;
			shell.dispose();
	
			
  		
  		
  		}
	});
  	btnSave.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
			
			if(e.keyCode == SWT.ARROW_RIGHT);
			{
				//buttonCancel.setFocus();
				btnCancel.setFocus();
			}

			if(e.keyCode == SWT.ARROW_UP)
			{
				txtopeningbal.setFocus();
			}
		}
		
	});
	btnCancel.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyReleased(arg0);
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtopeningbal.setFocus();
			}			
			else
			{						
				arg0.doit = false;
			}
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnSave.setFocus();
			}
		}
	});

      btnCancel.addListener(SWT.Selection, new Listener() {
          public void handleEvent(Event event) {
            //value = null;
        	  //MessageBox msgconfirm = new MessageBox(new Shell(),SWT.YES| SWT.NO|SWT.ICON_QUESTION);
        		CustomDialog msgconfirm = new CustomDialog(new Shell());
     
        	  msgconfirm.SetMessage("Are you sure?");
				int answer = msgconfirm.open();
				if (answer== SWT.YES) 
				{
					  cancelflag = true;
					  shell.dispose();
				}
				else
				{
					return;
				}
            
          }
        });
        shell.open();

  
	    Display display = parent.getDisplay();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch())
	        display.sleep();
	    }
	return value;
	}

 
  private Device getDisplay() {
	// TODO Auto-generated method stub
	return null;
}


  
  public static void main(String[] args) {
  
  }
}