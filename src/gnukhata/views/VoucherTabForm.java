package gnukhata.views;

/*@ Author
Vinay khedekar < vinay.itengg@gmail.com>
*/
import gnukhata.globals;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class VoucherTabForm extends Composite
{
	static String strOrgName;
	static String strFromYear;
	static String strToYear;
	public static String typeFlag;
	static Display display;
	public static TabFolder tfTransaction;
	public static TabItem tinewvoucher;
	public static TabItem tifdrecord ;
	Label lblLine;
	Label lbldate;
	Text txtddate;
	Label dash1;
	Text txtmdate;
	Label dash2;
	Text txtyrdate;
	Label lblvoucherno;
	Text txtvoucherno;
	Table table;
	TableItem tableitem0;
	TableItem tableitem1;
	TableItem tableitem2;
	TableItem tableitem3;
	TableColumn column0;
	TableColumn column1;
	TableColumn column2;
	TableColumn column3;
	Label lblDR_CR;
	Label lblAccName;
	Label lblDrAmount;
	Label lblcrAmount;
	TableColumn column;
	TableEditor dr_crEditor;
	TableEditor accNameEditor;
	TableEditor debAmtEditor;
	TableEditor creAmtEditor;
	TableEditor editor;
	TableEditor editor1;
	TableEditor editor2;
	TableEditor editor3;
	TableEditor editor4;
	TableEditor editor5;
	TableEditor editor6;
	TableEditor editor7;
	
	TableEditor editor8;
	TableEditor editor9;
	TableEditor editor10;
	Text txtDr_DebAmt;
	Text txtCr_DebAmt;
	Text txtDr_CrdAmt;
	Text txtCr_CrdAmt;
	Text txttotalDebAmt;
	Text txttotalCrAmt;
	Label lblTotal;
	Label lblnarration;
	Text txtnarration;
	Label lblselprj ;
	Combo comboselprj;
	Button save;
	Label lblsearchRec;
	Combo combosearchRec;
	Label lbleditvoucherno;
	Text txteditvoucherno;
	Text txtFromDt;
	Label lblFromDtDash1;
	Combo comboDr_cr;
	Combo comboCr_Dr;
	Combo comboDr_accName;
	Combo comboCr_accName;
	Label lblvFromDt;
	Text txtFromDt1;
	Label lblFromDtDash2;
	Text txtFromDt2;
	Label lblToDt;
	Text txtToDt;
	Label lblToDtDash1;
	Text txtToDt1;
	Label lblToDtDash2;
	Text txtToDt2;
	Label lblentamount;
	Text txtentamount;
	Button btndelete;
	Button btnsearch;
	Table vdtable;
	

	Vector<Object> params;
	AddNewVoucherComposite addnewvoucher;
	FindandEditVoucherComposite fdvoucher;
	
	public VoucherTabForm(Composite parent,int style)
	{
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
	
		/*Label lblHeadline = new Label(this,SWT.None);
		lblHeadline.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
		lblHeadline.setText("GNUKhata: A Free and Open Source Accounting Software");
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(51);
		layout.bottom = new FormAttachment(5);
		lblHeadline.setLayoutData(layout);*/
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(90);
		layout.bottom = new FormAttachment(10);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		

		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&")+"\n"+" Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(62);
		layout.bottom = new FormAttachment(7);
		lblOrgDetails.setLayoutData(layout);
		

		lblLine = new Label(this, SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,5);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(99);
		lblLine.setLayoutData(layout);
	
		
			
		
		tfTransaction =new TabFolder(this, SWT.NONE);
		layout =new FormData();
		layout.top = new FormAttachment(15);
		layout.left = new FormAttachment(2);//12
		layout.right = new FormAttachment(98);//95
		layout.bottom = new FormAttachment(97);
		tfTransaction.setLayoutData(layout);
		
		tinewvoucher = new TabItem(tfTransaction, SWT.NONE);
		tinewvoucher.setText("                        &New Voucher                            ");
		Composite cmpnewvoucher = new Composite(tfTransaction,SWT.BORDER);
		cmpnewvoucher.setLayout(formlayout);
		tinewvoucher.setControl(cmpnewvoucher);
		
		tifdrecord = new TabItem(tfTransaction, SWT.NONE);
		tifdrecord.setText("                &Find Vouchers                   ");
		Composite cmpfdrecord = new Composite(tfTransaction, SWT.BORDER | SWT.V_SCROLL);
		cmpfdrecord.setLayout(formlayout);
		tifdrecord.setControl(cmpfdrecord);
	
		this.pack();
		this.getAccessible();
		this.setEvent();
}
	public void setEvent()
	{	
		addnewvoucher = new AddNewVoucherComposite(tfTransaction, SWT.NONE, typeFlag);
		tinewvoucher.setControl(addnewvoucher);
		
		tfTransaction.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{	
				/*MessageBox msg = new MessageBox(new Shell(),SWT.OK);
				msg.setMessage("Enter into the tab event.....");
				msg.open();*/
				
				if (tfTransaction.getSelectionIndex() == 0)
				{
					addnewvoucher = new AddNewVoucherComposite(tfTransaction, SWT.NONE,typeFlag);
					tinewvoucher.setControl(addnewvoucher);
					addnewvoucher.txtvoucherno.setFocus();
				}
				if (tfTransaction.getSelectionIndex() == 1)
				{
					String searchValues[] = null;
					fdvoucher = new FindandEditVoucherComposite(tfTransaction, SWT.NONE,false,false,0,searchValues);
					tifdrecord.setControl(fdvoucher);
					fdvoucher.combosearchRec.setFocus();
				}
			}
		});
	}
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}

}