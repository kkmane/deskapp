package gnukhata.views;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.transactionController;
import gnukhata.controllers.reportmodels.transaction;

import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;

public class ViewDualLedgr extends Composite {

	/**
	 * @param args
	 * 
	 * 
	 * 
	 */
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color tableBackground;
	Color tableForeground;
	Color lightBlue;
	    String strOrgName;
		String strFromYear;
		String strToYear;
		String accname1;
		String oldaccName;
		Group ledger_report1;
		Group ledger_report2;
		
		TableViewer ledgerReport1;
		TableViewerColumn colDate1;
		TableViewerColumn colParticulars1;
		TableViewerColumn colVoucherNumber1;
		TableViewerColumn colDr1;
		TableViewerColumn colCr1;
		TableViewerColumn colNarration1;
		
		TableViewer ledgerReport2;
		TableViewerColumn colDate2;
		TableViewerColumn colParticulars2;
		TableViewerColumn colVoucherNumber2;
		TableViewerColumn colDr2;
		TableViewerColumn colCr2;
		TableViewerColumn colNarration2;
		ODPackage sheetStream, sheetStreamNar;
		int ledger_report1Width=0;
		int ledger_report1Height=0;
		int ledger_report2Width=0;
		int ledger_report2Height=0;
		int counter=0;
		int tblIndexLedger1;
		int tblIndexLedger2;
    /*	Table ledgerReport2;
		Table ledgerReport1;
		TableColumn lr1_Date;
		TableColumn lr1_Particulars;
		TableColumn lr1_VoucherNumber;
		TableColumn lr1_Dr;
		TableColumn lr1_Cr;
		TableColumn lr1_Narration;
		TableColumn lr2_Date;
		TableColumn lr2_Particulars;
		TableColumn lr2_VoucherNumber;
		TableColumn lr2_Dr;
		TableColumn lr2_Cr;
		TableColumn lr2_Narration;
		
		TableItem headerRow1;
		TableItem headerRow2;*/
     	Label lblOrgDetails;
		Label lblheadline;
		Label lblorgname;
		Label lr1_lblDate;
		Label lr1_lblParticulars;
		Label lr1_lblVoucherNumber;
		Label lr1_lblNarration;
		Label lr1_lblDr;
		Label lr1_lblCr;
		
		Label lr2_lblDate;
		Label lr2_lblParticulars;
		Label lr2_lblVoucherNumber;
		Label lr2_lblNarration;
		Label lr2_lblDr;
		Label lr2_lblCr;
		
		
		Label lblLogo;
		Label lblLink ;
		Label lblLine;
		Label lblPageName;
		Label lblPageName2;
		Label lblPeriod;
		
		Label lblPrjName;
		Label lblPrjName1;
/*		
		TableEditor lr1_DateEditor;
		TableEditor lr1_ParticularEditor;
		TableEditor lr1_VoucherNumberEditor;
		TableEditor lr1_DrEditotr;
		TableEditor lr1_CrEditor;
		TableEditor lr1_NarrationEditor;
		
		TableEditor lr2_DateEditor;
		TableEditor lr2_ParticularEditor;
		TableEditor lr2_VoucherNumberEditor;
		TableEditor lr2_DrEditotr;
		TableEditor lr2_CrEditor;
		TableEditor lr2_NarrationEditor;
*/		static Display display;
		
		Button btnViewLedgerForPeriod;
		Button btnViewLedgerForAccount;
		Button btnPrint;
		
		String strFromDate1;
		String strToDate1;
		String strFromDate;
		String strToDate;
		ArrayList<Button> voucherCodes = new ArrayList<Button>();
		String tb1;
		String accountName1;
		String startDate="";
		String endDate="";
		String projectName;
		String ledgerProject;
		boolean narrationflag;
		boolean narration;
		boolean tbDrilldown;
		boolean psDrilldown;
		boolean dualflag1;
		boolean tbflag;
		boolean projectflag;
		boolean ledgerflag;
		String oldaccName1;
		String oldprojectName1;
		String oldfromdate1;
		String oldenddate1;
		String oldselectproject1;
		boolean narration1;
		int shellwidth = 0;
		int finshellwidth;
		Color bgtblColor;
		Color fgtblColor;
		Color bgbtnColor;
		Color fgbtnColor;
	
		Vector<Object> printLedgerData = new Vector<Object>();
		// ViewDualLedgr(grandParent, SWT.None, result_t2,oldprojectName, narration, oldaccName,oldfromdate,oldenddate,tbflag,projectflag,tb,oldselectproject,dualflag);
			
		public ViewDualLedgr(Composite parent,int style,ArrayList<transaction> lstLedger1,ArrayList<transaction> lstLedger2,String ProjectName,String oldprojectName,boolean narrationFlag,boolean narration,String accountName,String oldaccName,String fromDate,String oldfromdate,String toDate,String oldenddate,boolean tbDrillDown,boolean tbflag,boolean psdrilldown,boolean projectflag,String tbType,String tb,String selectproject,String oldselectproject,boolean dualledgerflag,boolean dualflag)
		{
			super(parent, style);
			// TODO Auto-generated constructor stub
			strOrgName = globals.session[1].toString();
			strFromYear =  globals.session[2].toString();
			strToYear =  globals.session[3].toString();
			//txtaccname.setText(result[0].toString());
			
			/*MessageBox msg=w MessageBox(new Shell(),SWT.OK);
			msg.setMessage("Project name"+selectproject);
			msg.open();*/
			
			accountName1=accountName;
			projectName=ProjectName;
			narrationflag=narrationFlag;
			startDate=fromDate;
			endDate=toDate;
			tbDrilldown=tbDrillDown;
			psDrilldown=psdrilldown;
			tb1 = tbType;
			projectName=selectproject;
			ledgerProject=ProjectName;
			dualflag1=dualledgerflag;
			
			//oldvalues
			oldaccName1=oldaccName;
			oldprojectName1=oldprojectName;
			oldfromdate1=oldfromdate;
			oldenddate1=oldenddate;
			oldselectproject1=oldselectproject;
			narration1=narration;
			
			
			
			
			FormLayout formLayout= new FormLayout();
			this.setLayout(formLayout);
		    FormData layout =new FormData();
		    strFromDate1=oldfromdate.substring(8)+"-"+oldfromdate.substring(5,7)+"-"+oldfromdate.substring(0,4);
			strToDate1=oldenddate.substring(8)+"-"+oldenddate.substring(5,7)+"-"+oldenddate.substring(0,4);
			strFromDate=fromDate.substring(8)+"-"+fromDate.substring(5,7)+"-"+fromDate.substring(0,4);
			strToDate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0,4);
			
		  
			
			//Label lblLogo = new Label(this, SWT.None);
			//layout = new FormData();
			//layout.top = new FormAttachment(1);
			//layout.left = new FormAttachment(63);
			//layout.right = new FormAttachment(87);
			//layout.bottom = new FormAttachment(9);
			//layout.right = new FormAttachment(95);
			//layout.bottom = new FormAttachment(18);
			//lblLogo.setSize(getClientArea().width, getClientArea().height);
			//lblLogo.setLocation(getClientArea().width, getClientArea().height);
			//lblLogo.setLayoutData(layout);
			//Image img = new Image(display,"finallogo1.png");
			//lblLogo.setImage(globals.logo);
			
			Label lblOrgDetails = new Label(this,SWT.NONE);
			lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
			lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
			layout = new FormData();
			layout.top = new FormAttachment(0);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(53);
			layout.bottom = new FormAttachment(3);
			lblOrgDetails.setLayoutData(layout);

			Label lblOrgDetails1 = new Label(this,SWT.NONE);
			lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
			lblOrgDetails1.setText(" Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
			layout = new FormData();
			layout.top = new FormAttachment(0);
			layout.left = new FormAttachment(70);
			layout.right = new FormAttachment(99);
			layout.bottom = new FormAttachment(3);
			lblOrgDetails1.setLayoutData(layout);

			/*Label lblLink = new Label(this,SWT.None);
			lblLink.setText("www.gnukhata.org");
			lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(lblLogo,0);
			layout.left = new FormAttachment(65);
			//layout.right = new FormAttachment(33);
			//layout.bottom = new FormAttachment(19);
			lblLink.setLayoutData(layout);*/
			 
			Label lblLine = new Label(this,SWT.NONE);
			lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
			layout = new FormData();
			layout.top = new FormAttachment(2);
			layout.left = new FormAttachment(2);
			layout.right = new FormAttachment(99);
			layout.bottom = new FormAttachment(5);
			lblLine.setLayoutData(layout);
			

			
			
			lblheadline=new Label(this, SWT.NONE);
			lblheadline.setText(globals.session[1].toString().replace("&", "&&"));
			lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
			layout = new FormData();
			layout.top = new FormAttachment(lblLine,1);
			layout.left = new FormAttachment(45);
			lblheadline.setLayoutData(layout);
			
			lblPrjName=new Label(this, SWT.NONE);
			lblPrjName.setFont( new Font(display,"Times New Roman", 12, SWT.NORMAL | SWT.BOLD) );
			//lblPrjName.setText("Project: "+ProjectName);
			layout = new FormData();
			layout.top = new FormAttachment(lblheadline,1);
			layout.left = new FormAttachment(2);

			lblPrjName.setLayoutData(layout);
			if(ProjectName=="No Project" || ProjectName=="")
			{
				lblPrjName.setVisible(false);
			}
			else
			{
				lblPrjName.setText("Project: "+ProjectName.toString().replace("&", "&&"));
			}
			//System.out.print("Project name is dual :"+ProjectName);
			
			
			lblPrjName1=new Label(this, SWT.NONE);
			lblPrjName1.setFont( new Font(display,"Times New Roman", 12, SWT.NORMAL | SWT.BOLD) );
			//lblPrjName.setText("Project: "+ProjectName);
			layout = new FormData();
			layout.top = new FormAttachment(lblheadline,1);
			layout.left = new FormAttachment(60);
			lblPrjName1.setLayoutData(layout);
			if(oldprojectName.equals("No Project") || oldprojectName.equals(""))
			{
				lblPrjName1.setVisible(false);
			}
			else
			{
				lblPrjName1.setText("Project: "+oldprojectName.toString().replace("&", "&&"));
			}
			
			Label lblAccName=new Label(this, SWT.NONE);
			lblAccName.setFont( new Font(display,"Times New Roman", 12, SWT.BOLD ) );
			lblAccName.setText("Account Name: "+accountName1.toString().replace("&", "&&"));
			layout = new FormData();
			layout.top = new FormAttachment(lblPrjName,1);
			layout.left = new FormAttachment(2);
			lblAccName.setLayoutData(layout);
			
			Label lblAccName2=new Label(this, SWT.NONE);
			lblAccName2.setFont( new Font(display,"Times New Roman", 12, SWT.BOLD ) );
			lblAccName2.setText("Account Name: "+oldaccName1.toString().replace("&", "&&"));
			layout = new FormData();
			layout.top = new FormAttachment(lblPrjName1,1);
			layout.left = new FormAttachment(60);
			layout.right = new FormAttachment(98);
			lblAccName2.setLayoutData(layout);
			
			lblPageName = new Label(this, SWT.NONE);
			//lblPageName.setText("Ledger for account : "+ accountName );
			lblPageName.setFont(new Font(display, "Times New Roman", 12, SWT.NORMAL ));
			lblPageName.setText("Period From "+strFromDate1+" To "+strToDate1);
			layout = new FormData();
			layout.top = new FormAttachment(lblAccName,1);
			layout.left = new FormAttachment(60);
			layout.right = new FormAttachment(98);
			lblPageName.setLayoutData(layout);

			lblPageName2 = new Label(this, SWT.NONE);
			//lblPageName.setText("Ledger for account : "+ accountName );
			lblPageName2.setFont(new Font(display, "Times New Roman", 12, SWT.NORMAL ));
			lblPageName2.setText("Period From "+strFromDate+" To "+strToDate);
			layout = new FormData();
			layout.top = new FormAttachment(lblAccName2,1);
			layout.left = new FormAttachment(2);
			lblPageName2.setLayoutData(layout);
			
			ledger_report1 = new Group(this, SWT.BORDER);
			FormData  entry = new FormData();
			entry.left = new FormAttachment(0);
			entry.right = new FormAttachment(50);
			entry.top =  new FormAttachment(lblPageName, 5);
			entry.bottom = new FormAttachment(90);
			ledger_report1.setLayoutData(entry);
			ledger_report1.setLayout(new FormLayout());
			
			
			ledger_report2 = new Group(this, SWT.BORDER);
			FormData  entry1 = new FormData();
			entry1.left = new FormAttachment(ledger_report1,1);
			entry1.right = new FormAttachment(100);
			entry1.top =  new FormAttachment(lblPageName2, 5);
			entry1.bottom = new FormAttachment(90);
			ledger_report2.setLayoutData(entry1);
			ledger_report2.setLayout(new FormLayout());
		
			
					btnViewLedgerForAccount=new Button(this,SWT.PUSH);
			if(psdrilldown == false)
			{
				btnViewLedgerForAccount.setText("&View Another Ledger");
				 btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
			if(tbDrillDown == false)
			{
				btnViewLedgerForAccount.setText("&View Another Ledger");
				btnViewLedgerForAccount.setData("tbdrilldown",tbDrilldown );
			}
			else
			{
				btnViewLedgerForAccount.setData("tbdrilldown",tbDrilldown );
				btnViewLedgerForAccount.setData("enddate",toDate  );
				btnViewLedgerForAccount.setData("tbtype", tbType  );
				btnViewLedgerForAccount.setText("&Back to Trial Balance ");
			}
			
			
			}
			else
			{
				btnViewLedgerForAccount.setData("psdrilldown",psdrilldown );
				btnViewLedgerForAccount.setData("enddate",toDate  );
				btnViewLedgerForAccount.setData("selectproject", selectproject  );
				btnViewLedgerForAccount.setText("&Back to Project Statement ");
			}
			
			btnViewLedgerForAccount.setFont(new Font(display, "Times New Roman", 12,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(93);
			layout.left=new FormAttachment(40);
			layout.right = new FormAttachment(60);
			layout.bottom= new FormAttachment(98);
			btnViewLedgerForAccount.setLayoutData(layout);
			//btnViewLedgerForAccount.setFocus();
			
		  
		    //this.makeaccessible(ledgerReport1);
		    this.getAccessible();
		    this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		    
		    ledger_report1Width = ledger_report1.getClientArea().width;
			ledger_report1Height = ledger_report1.getClientArea().height;
			
			ledger_report2Width = ledger_report1.getClientArea().width;
		    ledger_report2Height = ledger_report1.getClientArea().height;
				
			
		    ledgerReport1 = new TableViewer(ledger_report1, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		    ledgerReport1.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		    ledgerReport1.getTable().setLinesVisible (true);
			ledgerReport1.getTable().setHeaderVisible (true);
			layout = new FormData();
			layout.top = new FormAttachment(0);
			if(narrationflag)
			{
				layout.left = new FormAttachment(0);
				layout.right = new FormAttachment(100);
			}
			else
			{
				layout.left = new FormAttachment(0);
				layout.right = new FormAttachment(100);
				
			}
			layout.bottom = new FormAttachment(100);
			ledgerReport1.getTable().setLayoutData(layout);
			
			ledgerReport2 = new TableViewer(ledger_report2, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		    ledgerReport2.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		    ledgerReport2.getTable().setLinesVisible (true);
			ledgerReport2.getTable().setHeaderVisible (true);
			layout = new FormData();
			layout.top = new FormAttachment(0);
			if(narration1)
			{
				layout.left = new FormAttachment(0);
				layout.right = new FormAttachment(99);
			}
			else
			{
				layout.left = new FormAttachment(0);
				layout.right = new FormAttachment(100);
				
			}
			layout.bottom = new FormAttachment(100);
			ledgerReport2.getTable().setLayoutData(layout);
			

			 this.makeaccessible(ledger_report1);
			 this.makeaccessible(this);
			 
			 this.makeaccessible(ledger_report2);
			 this.makeaccessible(this);
			
			
			    BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
				Background =  new Color(this.getDisplay() ,220 , 224, 227);
				Foreground = new Color(this.getDisplay() ,0, 0,0 );
				FocusBackground  = new Color(this.getDisplay(),78,97,114 );
				FocusForeground = new Color(this.getDisplay(),255,255,255);
				tableBackground = new Color(this.getDisplay(),255, 255, 214);
				tableForeground =  new Color(this.getDisplay(),184, 255, 148);
				lightBlue = new Color(this.getDisplay(),215,242,251);
				globals.setThemeColor(this, Background, Foreground);
				ledgerReport1.getControl().setBackground(lightBlue);
				ledgerReport2.getControl().setBackground(lightBlue);
		        globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
				globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		        //globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
				globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

				  //ledgerReport1.getTable().setBackground(FocusBackground);
				  //ledgerReport1.getTable().setForeground(FocusForeground);
			 /*bgbtnColor=this.btnViewLedgerForAccount.getBackground();
			 fgbtnColor=this.btnViewLedgerForAccount.getForeground();
			 bgtblColor=this.btnViewLedgerForAccount.getBackground();
			 fgtblColor=this.btnViewLedgerForAccount.getForeground();
			 
			*/
				 this.setReport(lstLedger1,lstLedger2, dualflag);
				 setEvents(lstLedger1,lstLedger2);		
		}	
			
		private void setReport(ArrayList<transaction> lstLedger1,ArrayList<transaction> lstLedger2,boolean narrationFlag)
		{
			
			colDate1 = new TableViewerColumn(ledgerReport1,SWT.NONE );
			colDate1.getColumn().setWidth(12 * ledger_report1Width/ 100 );
			colDate1.getColumn().setText("Date");
			colDate1.getColumn().setAlignment(SWT.CENTER);
			colDate1.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getTransactionDate();
					
					//return super.getText(element);
				}
				
			});
			
			
			colVoucherNumber1 = new TableViewerColumn(ledgerReport1,SWT.NONE );
			colVoucherNumber1.getColumn().setWidth(7 * ledger_report1Width/ 100 );
			colVoucherNumber1.getColumn().setAlignment(SWT.CENTER);
			colVoucherNumber1.getColumn().setText("V. No.");
			colVoucherNumber1.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getVoucherNo();
					
					//return super.getText(element);
				}
				
			});
			
			
			
			colParticulars1 = new TableViewerColumn(ledgerReport1,SWT.NONE );
			colParticulars1.getColumn().setWidth(46 * ledger_report1Width/ 100 );
			colParticulars1.getColumn().setText("Particulars");
			colParticulars1.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getParticulars();
					
					//return super.getText(element);
				}
				
			});
			
			colDr1 = new TableViewerColumn(ledgerReport1,SWT.NONE );
			colDr1.getColumn().setWidth(15 * ledger_report1Width/ 100 );
			colDr1.getColumn().setText("Dr");
			colDr1.getColumn().setAlignment(SWT.RIGHT);
			colDr1.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getDr();
					
					//return super.getText(element);
				}
				
			});
			

			colCr1 = new TableViewerColumn(ledgerReport1,SWT.NONE );
			colCr1.getColumn().setWidth(15 * ledger_report1Width/ 100 );
			colCr1.getColumn().setText("Cr");
			colCr1.getColumn().setAlignment(SWT.RIGHT);
			colCr1.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getCr();
					
					//return super.getText(element);
				}
				
			});
			

			colDate2 = new TableViewerColumn(ledgerReport2,SWT.NONE );
			colDate2.getColumn().setWidth(12 * ledger_report2Width/ 100 );
			colDate2.getColumn().setText("Date");
			colDate2.getColumn().setAlignment(SWT.CENTER);
			colDate2.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getTransactionDate();
					
					//return super.getText(element);
				}
				
			});
			
			
			colVoucherNumber2 = new TableViewerColumn(ledgerReport2,SWT.NONE );
			colVoucherNumber2.getColumn().setWidth(7 * ledger_report2Width/ 100 );
			colVoucherNumber2.getColumn().setText("V. No.");
			colVoucherNumber2.getColumn().setAlignment(SWT.CENTER);
			colVoucherNumber2.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getVoucherNo();
					
					//return super.getText(element);
				}
				
			});
			
			
			
			colParticulars2 = new TableViewerColumn(ledgerReport2,SWT.NONE );
			colParticulars2.getColumn().setWidth(46 * ledger_report2Width/ 100 );
			colParticulars2.getColumn().setText("Particulars");
			colParticulars2.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getParticulars();
					
					//return super.getText(element);
				}
				
			});
			
			colDr2 = new TableViewerColumn(ledgerReport2,SWT.NONE );
			colDr2.getColumn().setWidth(15 * ledger_report2Width/ 100 );
			colDr2.getColumn().setText("Dr");
			colDr2.getColumn().setAlignment(SWT.RIGHT);
			colDr2.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getDr();
					
					//return super.getText(element);
				}
				
			});
			

			colCr2 = new TableViewerColumn(ledgerReport2,SWT.NONE );
			colCr2.getColumn().setWidth(15 * ledger_report2Width/ 100 );
			colCr2.getColumn().setText("Cr");
			colCr2.getColumn().setAlignment(SWT.RIGHT);
			colCr2.setLabelProvider(new ColumnLabelProvider(){
				
				@Override
				public String getText(Object element) {
					// TODO Auto-generated method stub
					transaction row = (transaction) element;
					return row.getCr();
					
					//return super.getText(element);
				}
				
			});
			 ledgerReport1.setContentProvider(new ArrayContentProvider());
			 ledgerReport1.setInput(lstLedger1);
			 
			
			 //ledgerReport1.getTable().pack();
			
			 ledgerReport2.setContentProvider(new ArrayContentProvider());
			 ledgerReport2.setInput(lstLedger2);
			 TableItem[] items = ledgerReport1.getTable().getItems();
				for (int rowid=0; rowid<items.length; rowid++){
				    if (rowid%2==0) 
				    {
				    	items[rowid].setBackground(tableBackground);
				    }
				    else {
				    	items[rowid].setBackground(tableForeground);
				    }
				}
			 TableItem[] items1 = ledgerReport2.getTable().getItems();
				for (int rowid=0; rowid<items1.length; rowid++){
				    if (rowid%2==0) 
				    {
				    	items1[rowid].setBackground(tableBackground);
				    }
				    else {
				    	items1[rowid].setBackground(tableForeground);
				    }
				}
			 ledgerReport1.getTable().setSelection(0);
			 //ledgerReport2.getTable().pack();
			ledgerReport1.getTable().setFocus();
		}
		

	
						
 public void setEvents(final ArrayList<transaction> lstLedger1,final ArrayList<transaction> lstLedger2)
 {
	 
	 	ledgerReport1.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndexLedger1=ledgerReport1.getTable().getSelectionIndex();
				ledgerReport1.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				ledgerReport1.getTable().setSelection(tblIndexLedger1);
			}
		});
	 	
	 	ledgerReport2.getControl().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				tblIndexLedger2=ledgerReport2.getTable().getSelectionIndex();
				ledgerReport2.getTable().setSelection(-1);
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				ledgerReport2.getTable().setSelection(tblIndexLedger2);
			}
		});
	
	 
		btnViewLedgerForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewLedgerForAccount.getParent().getParent();
				if( (Boolean) btnViewLedgerForAccount.getData("psdrilldown") == false )
				{
				if( (Boolean) btnViewLedgerForAccount.getData("tbdrilldown") == false )
				{
					btnViewLedgerForAccount.getParent().dispose();
					
					ViewLedger vl=new ViewLedger(grandParent,SWT.NONE,"","","","",false,false,false,"","",false);
						vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String tbtype = btnViewLedgerForAccount.getData("tbtype").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showTrialBalance(grandParent, enddate, tbtype);
				}
				}

				else
				{
					String enddate = btnViewLedgerForAccount.getData("enddate").toString();
					String sp = btnViewLedgerForAccount.getData("selectproject").toString();
					btnViewLedgerForAccount.getParent().dispose();
					reportController.showProjectStatement(grandParent, enddate, sp);
				}
		}
		
		});
		ledgerReport1.getControl().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				ledgerReport2.getTable().select(-1);
				//super.focusGained(arg0);
				
			}
		});
		ledgerReport2.getControl().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				ledgerReport2.getTable().select(-1);
			}
		});

	
		
/*	btnViewLedgerForAccount.addFocusListener(new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusGained(arg0);
			btnViewLedgerForAccount.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
			btnViewLedgerForAccount.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_BLUE));
		}@Override
		public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusLost(arg0);
			btnViewLedgerForAccount.setBackground(bgbtnColor);
			btnViewLedgerForAccount.setForeground(fgbtnColor);
		}
	});
*/		ledgerReport1.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) ledgerReport1.getSelection();
				transaction dual = (transaction) selection.getFirstElement();
				try {
					if(dual.getParticulars().trim().equals("Total of Transactions") || dual.getParticulars().trim().equals("Grand Total")|| dual.getParticulars().trim().equals("Closing Balance c/f")|| dual.getParticulars().trim().equals("Opening Balance b/f")|| dual.getParticulars().equals(""))
					{
						return;
					}
					//String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
					Composite grandParent=(Composite) ledgerReport1.getTable().getParent().getParent().getParent();
					int vouchercode= dual.getVoucherCode();
					int lockflag = transactionController.getLockFlag(vouchercode);
					String searchValues[] = null; 
					transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,true, lockflag,0,searchValues);
					ledgerReport1.getTable().getParent().getParent().dispose();
					//ledgerReport2.getTable().getParent().dispose();
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//super.mouseDoubleClick(arg0);
			}
		});
		
		ledgerReport2.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) ledgerReport2.getSelection();
				transaction dual = (transaction) selection.getFirstElement();
				try {
					if(dual.getParticulars().trim().equals("Total of Transactions") || dual.getParticulars().trim().equals("Grand Total")|| dual.getParticulars().trim().equals("Closing Balance c/f")|| dual.getParticulars().trim().equals("Opening Balance b/f")|| dual.getParticulars().trim().equals(""))
					{
						return;
					}
					//String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
					Composite grandParent=(Composite) ledgerReport1.getTable().getParent().getParent().getParent();
					int vouchercode= dual.getVoucherCode();
					int lockflag = transactionController.getLockFlag(vouchercode);
					String searchValues[]=null;
					transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,true,lockflag,0,searchValues);
					ledgerReport1.getTable().getParent().getParent().dispose();
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//super.mouseDoubleClick(arg0);
			}
		});
		
		ledgerReport1.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
				{
					//drilldown here, make a call to voucherdetails.
					IStructuredSelection selection = (IStructuredSelection) ledgerReport1.getSelection();
					transaction dual = (transaction) selection.getFirstElement();
					try {
						if(dual.getParticulars().trim().equals("Total of Transactions") || dual.getParticulars().trim().equals("Grand Total")|| dual.getParticulars().trim().equals("Closing Balance c/f")|| dual.getParticulars().trim().equals("Opening Balance b/f")|| dual.getParticulars().trim().equals(""))
						{
							return;
						}
						//String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent=(Composite) ledgerReport1.getTable().getParent().getParent().getParent();
						int vouchercode= dual.getVoucherCode();
						int lockflag = transactionController.getLockFlag(vouchercode);
						String searchValues[] = null;
						transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,true,lockflag,0,searchValues);
						ledgerReport1.getTable().getParent().getParent().dispose();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//super.mouseDoubleClick(arg0);
				}
				}
			});
		
		ledgerReport2.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
				{
					//drilldown here, make a call to voucherdetails.
					IStructuredSelection selection = (IStructuredSelection) ledgerReport2.getSelection();
					transaction dual = (transaction) selection.getFirstElement();
					try {
						if(dual.getParticulars().trim().equals("Total of Transactions") || dual.getParticulars().trim().equals("Grand Total")|| dual.getParticulars().trim().equals("Closing Balance c/f")|| dual.getParticulars().trim().equals("Opening Balance b/f")|| dual.getParticulars().trim().equals(""))
						{
							return;
						}
						//String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent=(Composite) ledgerReport1.getTable().getParent().getParent().getParent();
						int vouchercode= dual.getVoucherCode();
						int lockflag = transactionController.getLockFlag(vouchercode);
						String searchValues[] =null;
						transactionController.showVoucherDetail(grandParent, "", vouchercode,false, tbflag, projectflag, tb1, true, startDate,oldfromdate1, endDate,oldenddate1, accountName1,oldaccName1, ledgerProject,oldprojectName1, narrationflag,narration1, projectName,oldselectproject1,true,lockflag,0,searchValues);
						ledgerReport1.getTable().getParent().getParent().dispose();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//super.mouseDoubleClick(arg0);
				}
				}
			});
		
		
 }
		 
		
		/*private void getAccessible() {
			// TODO Auto-generated method stub
			
		}*/

		public void makeaccessible(Control c) {
			/*
			 * getAccessible() method is the method of class Control which is the
			 * parent class of all the UI components of SWT including Shell.so when
			 * the shell is made accessible all the controls which are contained by
			 * that shell are made accessible automatically.
			 */
			c.getAccessible();
		}
		

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
